<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Icons extends MX_Controller {
	public function index()
	{
		$homepage = file_get_contents(base_url('theme/css/ca-icons.html'));
		$homepage = str_replace("../fonts", base_url('theme/fonts'), $homepage);
		$homepage = str_replace("../jquery-2.2.0.min.js", base_url('theme/admin/js/jquery.js'), $homepage);
		echo $homepage;
	}
}
