<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth
{
	public function __construct(){
		$this_ci =& get_instance();
		$this_ci->load->model('auth/Auth_model');
	}    
	
	function logged_in(){
		return (bool) $this->session->userdata('logged_in');
	}
	
	function user_data(){
		if($this->logged_in()){
			return $this->auth_model->handler_get($this->session->userdata('id_user'));
		}else{
			return false;
		}
	}
	
	function is_admin(){
		if(@$this->users->user_data()->group_type === 'admin'){
			return true;
		}else{
			return false;
		}
	}
}
