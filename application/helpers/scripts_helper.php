<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function br2nl($str) {
	$str = preg_replace("/(\r\n|\n|\r)/", "", $str);
	return preg_replace("/\<br(\s*)?\/?\>/i", "\n", $str);
}

function get_embed_video_link($link){
    $components = explode('=', $link);
    $video_id = $components[1];
    return 'https://youtube.com/embed/'.$video_id;
}

function getImage($img_path, $template_img = 'theme/images/no_image.png'){
	$path_parts = pathinfo($img_path);
	if(!empty($path_parts['extension']) && file_exists($img_path)){
		return $img_path;
	}else{
		return $template_img;
	}
}

function formatDate($date, $format = "d/m/Y H:i:s"){
    $str_to_time = strtotime($date);
	if($str_to_time && $date != '0000-00-00 00:00:00'){
	   return date($format, $str_to_time);
	} else{
		return '&mdash;';
	} 
}

function arrayByKey($arrays, $key, $multiple = false){
	$rez = array();
	if(empty($arrays)){
		return $rez;
	}
	
    foreach($arrays as $one){
		if(isset($one[$key])){
			if($multiple){
				$rez[$one[$key]][] = $one;
			} else{
				$rez[$one[$key]] = $one;
			}
		}
    }
    return $rez;
}

function generate_video_html($id, $type, $w, $h, $autoplay = 0){
	if(empty($id)) return false;

	switch($type) {
		case 'vimeo':
			return '<iframe class="player bd-none" src="//player.vimeo.com/video/'.$id.'?autoplay='.$autoplay.'&title=0&amp;byline=0&amp;portrait=0&amp;color=13bdab" width="'.$w.'" height="'.$h.'" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		break;
		case 'youtube':
			return  '<iframe class="player bd-none" width="'.$w.'" height="'.$h.'" src="//www.youtube.com/embed/'.$id.'?autoplay='.$autoplay.'" allowfullscreen></iframe>';
		break;
	}
}

function messageInModal($message, $type = 'errors'){
    $CI = &get_instance();
	$data['message'] = $message;
	$data['type'] = $type;
	echo $CI->load->view('messages_view', $data, true);
	exit();
}

function jsonResponse($message = '',$type = 'error', $additional_params = array()){
    $resp = $additional_params;
    $resp['mess_type'] = $type;
    $resp['mess_class'] = $type;
    $resp['message'] = $message;

    echo json_encode($resp);
    exit();
}

function jsonDTResponse($message = '',$additional_params = array(), $type = 'error'){
    $output = array(
		"iTotalRecords" => 0,
		"iTotalDisplayRecords" => 0,
		"aaData" => array()
    );

    $output = array_merge($output, $additional_params);
    $output['mess_type'] = $type;
    $output['message'] = $message;

    echo json_encode($output);
    exit();
}

function id_from_link($str, $to_int = true){
    $segments  = explode('-', $str);
	
	if($to_int){
    	return (int)end($segments);
	} else{
    	return end($segments);
	}
}

function remove_dir($dir,$action = 'delete') {
	if(empty($dir))
		return false;
	
	if(!is_dir($dir))
		return false;

	foreach(glob($dir . '/*') as $file) {
		@unlink($file);
	}
	
	if($action == 'delete')
		rmdir($dir);
}

function create_dir($dir, $mode = 0755){
    if(!is_dir($dir))
        mkdir ($dir, $mode, true);
}

function cut_str($str = "", $maxlength = 50){
	$trimmed_str = trim($str);
	return substr($trimmed_str, 0, $maxlength);
}

function text_elipsis($str = "", $maxlength = 50){
	$trimmed_str = trim($str);
	if(strlen($trimmed_str) > $maxlength){
		return substr($trimmed_str, 0, $maxlength).'...';
	}

	return $trimmed_str;
}

function get_contact_status($status = 0){
	$statuses = get_contact_statuses();
	if(array_key_exists($status, $statuses)){
		return $statuses[$status];
	} else{
		return array(
			'title' => 'Нейзвестный',
			'color_class' => 'default'
		);
	}
}

function get_contact_statuses(){
	return array(
		array(
			'title' => 'Новый',
			'color_class' => 'success'
		),
		array(
			'title' => 'В обработке',
			'color_class' => 'warning'
		),
		array(
			'title' => 'Выполнен',
			'color_class' => 'primary'
		),
		array(
			'title' => 'Аннулирован',
			'color_class' => 'danger'
		)
	);
}
?>
