<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>

<body>
    <div style="background: #ededed; width: 100%;">
        <table width="620" border="0" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0">
            <tr>
                <td style="height: 10px;">
                    <div style="height: 10px; width: 100%;"></div>
                </td>
            </tr>
            <tr>
                <td style="border:0px; padding: 15px; background: #ffffff;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0; padding: 0; background: #ffffff;">
                        <tr>
                            <td style="">
                                <table width="100%" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0;">
                                    <tr>
                                        <td>
                                            <p style="font: 16px Arial, sans-serif; color: #555555;">Добрый день,</p>
                                        </td>
                                    </tr>
                                </table>

                                <table width="100%" cellpadding="0" cellspacing="0" style="margin:0 auto; padding:0;">
                                    <tr>
                                        <td>
                                            <?php $this->load->view('email_templates/'.$email_content);?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding: 15px 0; font-family: Arial,sans-serif; vertical-align: moddle; text-align:center; color: #a2a2a2; font-size: 11px; line-height: 20px;">
                    &copy; <?php echo date('Y');?> vefasistem.md &mdash; Balti, Moldova
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
