<p style="font: 16px Arial, sans-serif; color: #555555;">
Для изменения пароля вашего личного кабинета просим вас пройти по <a href="<?php echo base_url('reset_password/'.@$reset_key);?>" style="color:#555555;">ссылке</a>.
</p>
<p style="font: 16px Arial, sans-serif; color: #555555;text-align:center;">
(Если ссылка не работает, скопируйте и вставьте в ваш браузер эту ссылку:) <br>
<?php echo base_url('reset_password/'.@$reset_key);?>
</p>