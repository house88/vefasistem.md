<p style="font: 16px Arial, sans-serif; color: #555555;margin-bottom:25px; margin-top:15px;">
    <?php echo $title;?>
</p>
<p style="font: 14px Arial, sans-serif; color: #555555;line-height:22px;">
    <?php if(!empty($contact_info['contact_name'])){?>
        <strong>Имя: </strong>
        <?php echo $contact_info['contact_name'];?>
        <br />    
    <?php }?>
    <?php if(!empty($contact_info['contact_email'])){?>
        <strong>Email: </strong>
        <?php echo $contact_info['contact_email'];?>
        <br />    
    <?php }?>
    <?php if(!empty($contact_info['contact_phone'])){?>
        <strong>Телефон: </strong>
        <?php echo $contact_info['contact_phone'];?>
        <br />    
    <?php }?>
    <?php if(!empty($contact_info['contact_date'])){?>
        <strong>Дата: </strong>
        <?php echo formatDate($contact_info['contact_date'], 'd/m/Y H:i:s');?>
        <br />    
    <?php }?>
    <?php if(!empty($contact_info['contact_text_client'])){?>
        <strong>Сообщение: </strong>
        <?php echo $contact_info['contact_text_client'];?>
        <br />    
    <?php }?>
</p>