<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('includes/header'); ?>
</head>
<body>
    <?php echo modules::run("menu"); ?>
    <?php echo modules::run("menu/site_menu"); ?>
    

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="banner_top"></div>
            </div>
            <div class="col-xs-12">
                <?php $this->load->view('includes/breadcrumbs'); ?>
            </div>
            <div class="col-xs-3">
                <?php echo modules::run("menu/left"); ?>
            </div>
            <div class="col-xs-9">
                <?php $this->load->view($main_content); ?>
            </div>
            <div class="col-xs-12"><hr></div>
        </div>
    </div>
    <?php $this->load->view('includes/footer'); ?>
</body>
</html>
