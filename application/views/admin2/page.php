<!DOCTYPE html>
<html lang="en">
    <head>
    <?php $this->load->view('admin/includes/header');?>
    </head>
    <body>
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <?php $this->load->view('admin/includes/nav_top');?>
                <?php echo modules::run("menu/admin_dashboard"); ?>
            </nav>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-xs-12">
                        <?php $this->load->view($main_content); ?>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->
        <?php $this->load->view('admin/includes/footer'); ?>
    </body>
</html>
