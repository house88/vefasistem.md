<div class="system-text-messages-b" style="<?php if(empty($system_messages)){echo 'display: none;';}?>">	
	<div class="text-b">
		<ul>
			<?php if(!empty($system_messages)){echo $system_messages;}?>
		</ul>
	</div>
</div>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url('/theme/admin');?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('/theme/admin');?>/bower_components/bootstrap-dialog/bootstrap-dialog.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url('/theme/admin');?>/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url('/theme/admin');?>/js/sb-admin-2.js"></script>