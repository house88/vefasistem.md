    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="<?php echo base_url('/theme');?>/favicon.ico" rel="icon" type="image/x-icon" />
	
    <title>Панель управления</title>
	
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('/theme/admin');?>/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url('/theme/admin');?>/bower_components/bootstrap-dialog/bootstrap-dialog.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url('/theme/admin');?>/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="<?php echo base_url('/theme/admin');?>/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url('/theme/admin');?>/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url('/theme/admin');?>/bower_components/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url('/theme/admin');?>/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url('/theme/vefasistem/fonts/ca-icons/style.css');?>" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
	<link rel="stylesheet" href="<?php echo base_url('/theme/admin');?>/bower_components/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
   
    <link href="<?php echo base_url('/theme/vefasistem/css');?>/sizes.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="<?php echo base_url('/theme/admin');?>/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo base_url('/theme/admin');?>/js/jquery.liTranslit.js"></script>
	<script type="text/javascript" src="<?php echo base_url('/theme/admin');?>/bower_components/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="<?php echo base_url('/theme/admin');?>/bower_components/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script>
		var base_url = '<?php echo base_url();?>';
	</script>
   
    <script src="<?php echo base_url(); ?>theme/admin/js/jquery.ui.widget.js"></script>
    <script src="<?php echo base_url(); ?>theme/admin/js/jquery.fileupload.js"></script>
	
    <!-- COLORPICKER -->
    <link href="<?php echo base_url();?>theme/admin/js/colorpicker/bootstrap-colorpicker.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url();?>theme/admin/js/colorpicker/bootstrap-colorpicker.js"></script>
    
    <!-- bootstrap-datetimepicker -->
    <link href="<?php echo base_url();?>theme/admin/js/bootstrap-datetimepicker/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url();?>theme/admin/js/bootstrap-datetimepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url();?>theme/admin/js/bootstrap-datetimepicker/locales/bootstrap-datepicker.ru.min.js"></script>
    
	<!-- SELECT2 -->
    <link href="<?php echo base_url();?>theme/admin/bower_components/select2/css/select2.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>theme/admin/bower_components/select2/css/select2.bootstrap.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url(); ?>theme/admin/bower_components/select2/js/select2.full.js"></script>
    
	<!-- DATATABLE -->
    <link href="<?php echo base_url();?>theme/admin/bower_components/datatable/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url(); ?>theme/admin/bower_components/datatable/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>theme/admin/bower_components/datatable/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>theme/admin/bower_components/datatable/js/jquery.dtFilters.js"></script>
    
	<!-- SWITCH -->
    <link href="<?php echo base_url();?>theme/admin/bower_components/switch/css/bootstrap-switch.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url(); ?>theme/admin/bower_components/switch/js/bootstrap-switch.js"></script>

	<!-- TOOLTIPSTER -->
    <link href="<?php echo base_url();?>theme/admin/bower_components/tooltipster/tooltipster.bundle.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url(); ?>theme/admin/bower_components/tooltipster/tooltipster.bundle.js"></script>

	<!-- CLIPBOARD -->
    <script src="<?php echo base_url(); ?>theme/admin/js/clipboard/clipboard.js"></script>
    
	<!-- iCheck -->
    <link href="<?php echo base_url();?>theme/admin/bower_components/icheck/skins/all.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url(); ?>theme/admin/bower_components/icheck/icheck.js"></script>
    <script src="<?php echo base_url(); ?>theme/admin/js/sortable.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url('theme/admin');?>/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            relative_urls: false,
            selector: ".description",
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontsizeselect | styleselect",
            toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview media fullpage ",
            image_advtab: true ,
			fontsize_formats: '8px 10px 12px 14px 16px 18px 20px 22px 24px 36px',
            external_filemanager_path:"/theme/admin/js/filemanager/",
            filemanager_title:"Responsive Filemanager" ,
            external_plugins: { "filemanager" : "/theme/admin/js/filemanager/plugin.min.js"}
        });
		
		$(function(){
			$(".select2").select2({
				width: 'resolve'
			});
			$('input').iCheck({
				checkboxClass: 'icheckbox_flat-blue',
				radioClass: 'iradio_flat-blue'
			}).on('ifClicked ifChanged ifChecked ifUnchecked check', function(event){                
                $(this).trigger('click');  
                $('input').iCheck('update');
            });
		});
    </script>
