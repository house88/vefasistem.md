<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/admin/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/vefasistem/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>theme/vefasistem/css/sizes.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo base_url();?>theme/vefasistem/js/jquery.js"></script>
    <script src="<?php echo base_url();?>theme/vefasistem/js/bootstrap.js"></script>
    <script src="<?php echo base_url();?>theme/admin/js/scripts.js"></script>
    <script>
        var base_url = '<?php echo base_url();?>';
    </script>
</head>
<body>
    <div class="auth-box">
        <!-- /.login-logo -->
        <div class="auth-box-body">
            <?php $this->load->view($main_content); ?>
        </div>
    </div>
    <!-- /.login-box -->
    <div class="system-text-messages-b" style="<?php if(empty($system_messages)){echo 'display: none;';}?>">	
        <div class="text-b">
            <ul>
                <?php if(!empty($system_messages)){echo $system_messages;}?>
            </ul>
        </div>
    </div>
</body>
</html>
