<section class="header-wr" id="header-wr">
    <div class="full-wr">
        <video poster="<?php echo base_url();?>theme/vefasistem/bg_video/bg_video.jpg" autoplay="" loop="" muted playsinline>
            <source src="<?php echo base_url();?>theme/vefasistem/bg_video/bg_video.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
            <source src="<?php echo base_url();?>theme/vefasistem/bg_video/bg_video.ogv" type='video/ogg; codecs="theora, vorbis"'>
            <source src="<?php echo base_url();?>theme/vefasistem/bg_video/bg_video.webm" type='video/webm; codecs="vp8, vorbis"'>
        </video>
        <div class="container-wr">
            <div class="container">
                <div class="row">
                    <span class="circle"></span>
                    <div class="col-xs-12 text-center">
                        <div class="logo-container">
                            <a href="<?php echo base_url();?>" class="logo">
                                <img src="<?php echo base_url();?>theme/vefasistem/css/images/vlogo-white.png" alt="logo">
                            </a>
                            <h1><?php lang_line('site_slogan', true);?></h1>
                        </div>
                    </div>
                    <a href="#our-products_wr" class="scroll-bottom js-scroll"><span></span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-products_wr" id="our-products_wr">
    <div class="full-wr"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="block-title"><?php lang_line('block_products_header', true);?></h2>
            </div>
            <?php $products = Modules::run('products/_get_all', array('product_active' => 1));?>
            <?php if(!empty($products)){?>
                <?php foreach($products as $product){?>
                    <div class="col-md-4 col-sm-6">
                        <div class="product-item">
                            <a href="#" data-href="<?php echo site_url('products/popup/show_product?product='.$product['id_product']);?>" class="call-popup" data-popup="general-popup">
                                <div class="product-img_wr">
                                    <span class="mask-hover"><?php lang_line('btn_more', true);?></span>
                                    <img src="<?php echo base_url();?>files/products/<?php echo $product['product_photo'];?>" alt="<?php echo $this->lang->get_lang_column('product_title', $product);?>">
                                </div>
                                <span class="product-name"><?php echo $this->lang->get_lang_column('product_title', $product);?></span>
                            </a>
                        </div>
                    </div>
                <?php }?>            
            <?php }?>
        </div>
    </div>
</section>

<section class="how-we-work_wr" id="how-we-work_wr">
    <div class="full-wr"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="block-title"><?php lang_line('block_how_we_work_header', true);?></h2>
            </div>
            <script>
                $(function(){
                    $('.step-block-wr').on('mouseenter mouseleave', function(){
                        $(this).find('.step-block-text').toggleClass('fadeIn');
                    });
                });
            </script>
            <?php $block_free_measurement = Modules::run('static_block/_get_block', array('alias' => 'free_measurement'));?>
            <div class="col-xs-12 col-sm-4 col-md-4 step-block-wr">
                <div class="img-wr">
                    <img src="<?php echo base_url();?>theme/vefasistem/images/zamer.png" alt="<?php echo $this->lang->get_lang_column('block_title', $block_free_measurement);?>">
                    <div class="step-block-text animated">
                        <span class="step-block-title"><?php echo $this->lang->get_lang_column('block_title', $block_free_measurement);?></span>
                        <div class="block-text">
                            <?php echo $this->lang->get_lang_column('block_text', $block_free_measurement);?>
                        </div>
                        <a href="#" data-href="<?php echo site_url('contacts/popup/callme');?>" class="btn btn3d btn-hot btn-sm call-popup" data-popup="contact_us"><?php lang_title('btn_order_free_measurement');?></a>
                    </div>
                </div>
                <a href="#" class="title-wr"><?php echo $this->lang->get_lang_column('block_title', $block_free_measurement);?></a>
            </div>
            <?php $block_own_production = Modules::run('static_block/_get_block', array('alias' => 'own_production'));?>
            <div class="col-xs-12 col-sm-4 col-md-4 step-block-wr">
                <div class="img-wr">
                    <img src="<?php echo base_url();?>theme/vefasistem/images/production.png" alt="<?php echo $this->lang->get_lang_column('block_title', $block_own_production);?>">
                    <div class="step-block-text animated">
                        <span class="step-block-title"><?php echo $this->lang->get_lang_column('block_title', $block_own_production);?></span>
                        <div class="block-text">
                            <?php echo $this->lang->get_lang_column('block_text', $block_own_production);?>
                        </div>
                    </div>
                </div>
                <a href="#" class="title-wr"><?php echo $this->lang->get_lang_column('block_title', $block_own_production);?></a>
            </div>
            <?php $block_quick_installation = Modules::run('static_block/_get_block', array('alias' => 'quick_installation'));?>
            <div class="col-xs-12 col-sm-4 col-md-4 step-block-wr">
                <div class="img-wr">
                    <img src="<?php echo base_url();?>theme/vefasistem/images/mount.png" alt="<?php echo $this->lang->get_lang_column('block_title', $block_quick_installation);?>">
                    <div class="step-block-text animated">
                        <span class="step-block-title"><?php echo $this->lang->get_lang_column('block_title', $block_quick_installation);?></span>
                        <div class="block-text">
                            <?php echo $this->lang->get_lang_column('block_text', $block_quick_installation);?>
                        </div>
                    </div>
                </div>
                <a href="#" class="title-wr"><?php echo $this->lang->get_lang_column('block_title', $block_quick_installation);?></a>
            </div>
            <div class="col-xs-12 btn-wr">
                <a href="#" data-href="<?php echo site_url('contacts/popup/callme');?>" class="btn btn3d btn-hot btn-extralg call-popup" data-popup="contact_us"><?php lang_title('btn_contact_us');?></a>
            </div>
        </div>
    </div>
</section>

<section class="windows_evolution_wr" id="windows_evolution_wr">
    <div class="full-wr" data-parallax="scroll" data-image-src="<?php echo base_url();?>theme/vefasistem/css/images/triangles_bg.png"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="block-title"><?php lang_line('block_windows_header', true);?></h2>
            </div>
            <div class="col-xs-12">
                <div class="windows_evolution_list-wr row row-centered">
                    <?php $windows = Modules::run('windows/_get_windows');?>
                    <?php foreach($windows as $window){?>
                        <div class="col-xs-12 col-md-3 col-sm-4 col-centered text-center">
                            <div class="window-item">
                                <a href="#" data-href="<?php echo site_url('windows/popup/show_window?window='.$window['id_window']);?>" class="call-popup" data-popup="general-popup">
                                    <span class="circle">
                                        <div class="square">
                                            <div class="content">
                                                <div class="table">
                                                    <div class="table-cell">
                                                        <p><?php lang_line('btn_more', true);?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                    <span class="product-image">
                                        <img src="<?php echo site_url('files/windows/'.$window['window_photo']);?>" alt="<?php echo $window[lang_column('window_title')];?>">
                                    </span>
                                    <span class="product-name"><?php echo $window[lang_column('window_title')];?></span>
                                    <span class="product-desc"><?php echo $window[lang_column('window_stext')];?></span>
                                </a>
                            </div>
                        </div>                    
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="trocal76_wr">
    <div class="full-wr">
        <div class="full-wr-bg"></div>
        <div class="full-wr-text">
            <?php $block_tocal_76_intro = Modules::run('static_block/_get_block', array('alias' => 'tocal_76_intro'));?>
            <h2 class="block-title"><?php echo $block_tocal_76_intro[lang_column('block_title')];?></h2>
            <?php echo $block_tocal_76_intro[lang_column('block_text')];?>
            <a href="#" class="btn btn3d btn-sky btn-extralg call-function" data-callback="play_video" data-index="1">Continue</a>
        </div>
    </div>
    <div class="video-wr">
        <video  preload="none" id="trocal76_player-element"></video>
    </div>
    <?php $videos = Modules::run('video/_get_videos');?>
    <div class="video_description">
        <?php if(!empty($videos)){?>
            <?php foreach($videos as $video){?>
                <div data-video="<?php echo $video['id_video'];?>">
                    <h2 class="block-title mt-0"><?php echo $video[lang_column('video_title')];?></h2>
                    <div class="video_description-html">
                        <?php echo $video[lang_column('video_text')];?>
                    </div>
                </div>        
            <?php }?>
        <?php }?>
        <div class="row">
            <div class="col-xs-12">
                <p><?php lang_line('form_contact_subtext', true);?></p>
                <form>
                    <div class="form-group">
                        <input class="form-control" placeholder="<?php lang_line('form_contact_name', true);?> *" name="name">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="<?php lang_line('form_contact_phone', true);?> *" name="phone">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="<?php lang_line('form_contact_email', true);?> *" name="email">
                    </div>
                    <div class="form-group text-left">
                        <label>
                            <input type="checkbox" name="add_comment" class="nice-input"> <?php lang_line('form_contact_leave_comment', true);?>
                        </label>
                    </div>
                    <div class="form-group contact_us_message" style="display:none;">
                        <textarea class="form-control" name="messages" cols="30" rows="5" placeholder="<?php lang_line('form_contact_comment', true);?>"></textarea>
                    </div>
                    <button type="submit" class="btn btn3d btn-sky btn-lg call-function" data-callback="callme_callback"><?php lang_line('form_contact_btn', true);?></button>
                </form>
            </div>
        </div>
    </div>
    <div class="video_menu" id="trocal76_menu">
        <ul>
            <li>
                <a href="#" class="call-function" data-callback="play_video" data-index="1"><span><?php echo lang_line('video_menu_large_surfaces', true);?></span> <i class="ca-icon ca-icon_sun"></i></a>
            </li>
            <li>
                <a href="#" class="call-function" data-callback="play_video" data-index="2"><span><?php echo lang_line('video_menu_heat_insulation', true);?></span> <i class="ca-icon ca-icon_snowflake"></i></a>
            </li>
            <li>
                <a href="#" class="call-function" data-callback="play_video" data-index="3"><span><?php echo lang_line('video_menu_noise_protection', true);?></span> <i class="ca-icon ca-icon_audio-volume"></i></a>
            </li>
            <li>
                <a href="#" class="call-function" data-callback="play_video" data-index="4"><span><?php echo lang_line('video_menu_safety', true);?></span> <i class="ca-icon ca-icon_security"></i></a>
            </li>
            <li>
                <a href="#" class="call-function" data-callback="play_video" data-index="5"><span><?php echo lang_line('video_menu_scope_for_design', true);?></span> <i class="ca-icon ca-icon_layout"></i></a>
            </li>
            <li>
                <a href="#" class="call-function" data-callback="play_video" data-index="6"><span><?php echo lang_line('video_menu_durability', true);?></span> <i class="ca-icon ca-icon_statistics"></i></a>
            </li>
            <li>
                <a href="#" class="call-function" data-callback="play_video" data-index="7"><span><?php echo lang_line('video_menu_ecology', true);?></span> <i class="ca-icon ca-icon_two-leaves"></i></a>
            </li>
            <li>
                <a href="#" class="call-function" data-callback="play_video" data-index="8"><span><?php echo lang_line('video_menu_at_a_glance', true);?></span> <i class="ca-icon ca-icon_checkered-flag"></i></a>
            </li>
        </ul>
    </div>
</section>

<section class="why-we_wr" id="why-we_wr">
    <div class="full-wr" data-parallax="scroll" data-image-src="<?php echo base_url();?>theme/vefasistem/css/images/office.jpg"></div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h2 class="block-title"><?php lang_line('block_our_achievements_header', true);?></h2>
            </div>
            <div class="col-xs-12 col-md-4 col-sm-6">
                <div class="count-item text-center">
                    <div class="number" id="countup1" data-target="countup1" data-number="<?php (int)lang_line('block_our_achievements_number_1_1', true);?>" data-count>0</div>
                    <div class="text"><?php lang_line('block_our_achievements_number_1', true);?></div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-sm-6">
                <div class="count-item text-center">
                    <div class="number" id="countup2" data-target="countup2" data-number="<?php (int)lang_line('block_our_achievements_number_2_1', true);?>" data-count>0</div>
                    <div class="text"><?php lang_line('block_our_achievements_number_2', true);?></div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-sm-6">
                <div class="count-item text-center">
                    <div class="number" id="countup3" data-target="countup3" data-number="<?php (int)lang_line('block_our_achievements_number_3_1', true);?>" data-count>0</div>
                    <div class="text"><?php lang_line('block_our_achievements_number_3', true);?></div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-sm-6">
                <div class="count-item text-center">
                    <div class="number" id="countup4" data-target="countup4" data-number="<?php (int)lang_line('block_our_achievements_number_4_1', true);?>" data-count>0</div>
                    <div class="text"><?php lang_line('block_our_achievements_number_4', true);?></div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-sm-6">
                <div class="count-item text-center">
                    <div class="number" id="countup5" data-target="countup5" data-number="<?php (int)lang_line('block_our_achievements_number_5_1', true);?>" data-count>0</div>
                    <div class="text"><?php lang_line('block_our_achievements_number_5', true);?></div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-sm-6">
                <div class="count-item text-center">
                    <div class="number" id="countup6" data-target="countup6" data-number="<?php (int)lang_line('block_our_achievements_number_6_1', true);?>" data-count>0</div>
                    <div class="text"><?php lang_line('block_our_achievements_number_6', true);?></div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="map-wr" id="map-wr">
    <div class="container">
        <div class="row equal vertical-bottom">
            <div class="col-xs-12 text-center">
                <h2 class="block-title"><?php lang_line('block_contacts_header', true);?></h2>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <p><?php lang_line('form_contact_subtext', true);?></p>
                <form>
                    <div class="form-group">
                        <input class="form-control" placeholder="<?php lang_line('form_contact_name', true);?> *" name="name">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="<?php lang_line('form_contact_phone', true);?> *" name="phone">
                    </div>
                    <div class="form-group">
                        <input class="form-control" placeholder="<?php lang_line('form_contact_email', true);?> *" name="email">
                    </div>
                    <div class="form-group text-left">
                        <label>
                            <input type="checkbox" name="add_comment" class="nice-input"> <?php lang_line('form_contact_leave_comment', true);?>
                        </label>
                    </div>
                    <div class="form-group contact_us_message" style="display:none;">
                        <textarea class="form-control" name="messages" cols="30" rows="5" placeholder="<?php lang_line('form_contact_comment', true);?>"></textarea>
                    </div>
                    <button type="submit" class="btn btn3d btn-sky btn-lg call-function" data-callback="callme_callback"><?php lang_line('form_contact_btn', true);?></button>
                </form>
            </div>
            <div class="hidden-xs col-sm-6 col-md-6 overflow-h text-right">
                <img src="<?php echo base_url();?>theme/vefasistem/images/contact-us.png" alt="" class="overflow-h">
            </div>
        </div>
        <div class="row equal list_locations_header-wr">
            <div class="col-xs-12">
                <h2 class="block-title mb-30"><?php lang_line('block_locations_header', true);?></h2>
            </div>
        </div>
        <div class="row equal list_locations-wr"></div>
    </div>
    <div class="map-element" id="mapid"></div>
    
    <script>
        var markers = <?php echo Modules::run('locations/_get_markers')?>;
        var scroll_to_element = function(btn){
            var $this = $(btn);
            var target_element = $this.data('element');
            var target_element_offset = ($this.data('offset'))?$this.data('offset'):0;
            $('html, body').animate({
                scrollTop: $(target_element).offset().top + target_element_offset
            }, 1000);
        }

        var open_marker_callback = function(element){
            var $this = $(element);
            var location_marker = $this.data('marker');
            $('html, body').animate({
                scrollTop: $('#mapid').offset().top
            }, 1000);
            markers[location_marker].marker.openPopup();
        }

        $(function(){
            var mymap = L.map('mapid', {
                center: [46.770685,27.99],
                zoom: 8,
                scrollWheelZoom: false
            });
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                attribution: '<a href="<?php echo base_url();?>" target="_blank">VEFASISTEM, </a><a href="http://www.openstreetmap.org/#map=15/47.770685/27.929283999999996" target="_blank">© OpenStreetMap</a>',
                maxZoom: 18,
                id: 'house88.7b4546ed',
                accessToken: 'pk.eyJ1IjoiaG91c2U4OCIsImEiOiJjaXlqeDh1amUwMDB2MndyNHV4NzQxbm0zIn0.bEbZ4H00DAZKEyM18j7Ozg'
            }).addTo(mymap);

            $.each(markers, function(index, marker_detail){                
                marker = L.marker([marker_detail.latitude, marker_detail.longitude]).addTo(mymap);
                marker_template = '<img src="<?php echo base_url();?>theme/vefasistem/css/images/logo.png" class="w-200" alt="logo"><br>\
                                    <p><b>or. '+marker_detail.city+',</b> <br>'+marker_detail.address+'<br>Тел: '+marker_detail.phone+'<br>Факс: '+marker_detail.fax+'<br>Email: '+marker_detail.email+'</p>';
                marker.bindPopup(marker_template);
                markers[index].marker = marker;
                if(index == 1){
                    marker.openPopup();
                }

                var template = '<div class="location-details col-xs-12 col-sm-6 col-md-4">\
                                    <i class="ca-icon ca-icon_marker"></i>\
                                    <div class="details">\
                                        <a class="display-only-mobile" href="http://www.google.com/maps/place/'+marker_detail.address+', '+marker_detail.city+'" target="_blank">\
                                            <p>Balti</p>\
                                        </a>\
                                        <p class="display-mobile-hidden call-function" data-callback="open_marker_callback" data-marker="'+index+'">'+marker_detail.city+'</p>\
                                        <div class="phone">\
                                            <i class="ca-icon ca-icon_smartphone"></i>\
                                            <span>'+marker_detail.phone+'</span>\
                                        </div>\
                                        <div class="address">\
                                            <i class="ca-icon ca-icon_marker"></i>\
                                            <span>\
                                                <a href="http://www.google.com/maps/place/'+marker_detail.address+', '+marker_detail.city+'" target="_blank">'+marker_detail.address+', '+marker_detail.city+'</a>\
                                            </span>\
                                        </div>\
                                    </div>\
                                </div>';
                $('.list_locations-wr').append(template);
            });
        });
    </script>
</section>