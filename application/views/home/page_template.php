<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('shop/includes/header_view'); ?>
</head>
<body>
    <?php $this->load->view('shop/includes/header_nav'); ?>
	<section id="wr-body">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?php $this->load->view('shop/includes/breadcrumbs'); ?>
				</div>
				<div class="col-xs-12">
					<?php $this->load->view($main_content); ?>
				</div>
			</div>
		</div>
	</section>
    <?php $this->load->view('shop/includes/footer_view'); ?>
</body>
</html>
