<header id="header_nav" class="header_nav-wr">
	<div class="nav-show-menu">
		<span></span>
		<span></span>
		<span></span>
	</div>
	<nav class="header-menu js-menu">
		<div class="logo-menu">
			<img src="<?php echo base_url();?>theme/vefasistem/css/images/vlogo.png" alt="VEFASISTEM">
		</div>
		<ul class="navigation js-menu show">
			<li><a href="#header-wr" class="js-scroll"><?php lang_line('site_menu_home', true);?></a></li>
			<li><a href="#our-products_wr" class="js-scroll"><?php lang_line('site_menu_products', true);?></a></li>
			<li><a href="#how-we-work_wr" class="js-scroll"><?php lang_line('site_menu_how_we_work', true);?></a></li>
			<li><a href="#windows_evolution_wr" class="js-scroll"><?php lang_line('site_menu_windows', true);?></a></li>
			<li class="hidden-max-1200"><a href="#trocal76_menu" class="js-scroll"><?php lang_line('site_menu_window_video', true);?></a></li>
			<li><a href="#why-we_wr" class="js-scroll"><?php lang_line('site_menu_our_achievements', true);?></a></li>
			<li><a href="#map-wr" class="js-scroll"><?php lang_line('site_menu_contacts', true);?></a></li>
		</ul>		
		<div class="header_menu_langs">
			<ul>
				<li>
					<a href="<?php echo base_url();?>">Ro</a>
				</li>
				<li>
					<a href="<?php echo base_url('ru');?>">Ru</a>
				</li>
				<li>
					<a href="<?php echo base_url('en');?>">En</a>
				</li>
			</ul>
		</div>
		<ul class="el-social">
			<li><a href="https://facebook.com/" class="social-fb" target="_blank"></a></li>
			<li><a href="https://ok.ru/" class="social-ok" target="_blank"></a></li>
			<li><a href="https://vk.com/" class="social-vk" target="_blank"></a></li>
		</ul>
	</nav>
	<div class="logo-header">
		<img src="<?php echo base_url();?>theme/vefasistem/css/images/logo.png" alt="VEFASISTEM">
		<span><?php lang_line('site_slogan', true);?></span>
	</div>
	<div class="header-right">
		<div class="header-phone">
			<a href="tel:+373 (22) 95 55 55" class="call-function" data-callback="scroll_to_element" data-element=".list_locations_header-wr" data-offset="-100">+373 (22) 95 55 55</a>
			<a href="tel:+373 (231) 9 40 30" class="call-function" data-callback="scroll_to_element" data-element=".list_locations_header-wr" data-offset="-100">+373 (231) 9 40 30</a>
		</div>
		<div class="header_langs">
			<ul>
				<li class="full-width">
					<a href="#" data-href="<?php echo site_url('contacts/popup/dealer');?>" class="call-popup" data-popup="contact_us"><?php lang_title('header_link_become_dealer');?></a>
				</li>
				<li>
					<a href="<?php echo base_url('en');?>">En</a>
				</li>
				<li>
					<a href="<?php echo base_url('ru');?>">Ru</a>
				</li>
				<li>
					<a href="<?php echo base_url();?>">Ro</a>
				</li>
			</ul>
		</div>
	</div>
</header>