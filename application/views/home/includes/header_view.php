<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Cravciuc Andrei - Senior Deweloper">
    <base href="<?php echo base_url();?>">
    <title><?php echo ((isset($stitle))?$stitle:$settings['default_title']['setting_value']);?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="<?php echo ((isset($skeywords))?$skeywords:$settings['mk']['setting_value']);?>">
    <meta name="description" content="<?php echo ((isset($sdescription))?$sdescription:$settings['md']['setting_value']);?>">
    
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>theme/vefasistem/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>theme/vefasistem/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>theme/vefasistem/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>theme/vefasistem/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>theme/vefasistem/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>theme/vefasistem/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>theme/vefasistem/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>theme/vefasistem/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>theme/vefasistem/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url();?>theme/vefasistem/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>theme/vefasistem/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>theme/vefasistem/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>theme/vefasistem/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url();?>theme/vefasistem/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>theme/vefasistem/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <script>
        var base_url = '<?php echo base_url();?>';
        var site_lang = '<?php echo $this->lang->lang();?>';
    </script>

    <script src="<?php echo base_url();?>theme/vefasistem/js/jquery.js"></script>
    <script src="<?php echo base_url();?>theme/vefasistem/js/bootstrap.min.js"></script>

    <!--PLUGINS-->
        <link rel="stylesheet" href="<?php echo base_url();?>theme/vefasistem/plugins/leaflet/leaflet.css" />
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/leaflet/leaflet.js"></script>

        <link rel="stylesheet" href="<?php echo base_url();?>theme/vefasistem/plugins/remodal/remodal.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>theme/vefasistem/plugins/remodal/remodal-theme.css" />
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/remodal/remodal.js"></script>
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/parallax/parallax.js"></script>
        <!-- iCheck -->
        <link href="<?php echo base_url();?>theme/vefasistem/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/icheck/icheck.js"></script>
        <!--iCheck END-->
        <!-- bxslider -->
        <link href="<?php echo base_url();?>theme/vefasistem/plugins/bxslider/bxslider.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/bxslider/bxslider.js"></script>
        <!--bxslider END-->
        <!-- bxslider -->
        <link href="<?php echo base_url();?>theme/vefasistem/plugins/plyr/plyr.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/plyr/plyr.js"></script>
        <script src="<?php echo base_url();?>theme/vefasistem/js/plyr.player.js"></script>
        <!--bxslider END-->
        <!-- slick -->
        <link href="<?php echo base_url();?>theme/vefasistem/plugins/slick/slick.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url();?>theme/vefasistem/plugins/slick/slick-theme.css" rel="stylesheet" type="text/css">
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/slick/slick.js"></script>
        <!--slick END-->
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/foreach/foreach.js"></script>
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/countup/countUp.js"></script>
        <script src="<?php echo base_url();?>theme/vefasistem/plugins/waypoints/waypoints.js"></script>
    <!--PLUGINS END-->

    <link href="<?php echo base_url();?>theme/vefasistem/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>theme/vefasistem/fonts/ca-icons/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>theme/vefasistem/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>theme/vefasistem/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>theme/vefasistem/css/sizes.css" rel="stylesheet">

    <script src="<?php echo base_url();?>theme/vefasistem/js/scripts.js"></script>