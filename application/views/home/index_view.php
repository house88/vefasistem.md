<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('home/includes/header_view'); ?>
</head>
<body>
    <div class="preloader-wrapper">
        <div class="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>

    <?php $this->load->view('home/includes/header_nav'); ?>

    <?php $this->load->view($main_content); ?>

    <?php $this->load->view('home/includes/footer_view'); ?>
</body>
</html>