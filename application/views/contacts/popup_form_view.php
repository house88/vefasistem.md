<div class="window-popup-content-wr">
    <div class="modal-desc"><?php lang_line('form_contact_subtext', true);?></div>
    <form>
        <div class="form-group">
            <input class="form-control" placeholder="<?php lang_line('form_contact_name', true);?> *" name="name">
        </div>
        <div class="form-group">
            <input class="form-control" placeholder="<?php lang_line('form_contact_phone', true);?> *" name="phone">
        </div>
        <div class="form-group">
            <input class="form-control" placeholder="<?php lang_line('form_contact_email', true);?> *" name="email">
        </div>
        <div class="form-group text-left">
            <label>
                <input type="checkbox" name="add_comment" class="nice-input"> <?php lang_line('form_contact_leave_comment', true);?>
            </label>
        </div>
        <div class="form-group contact_us_message" style="display:none;">
            <textarea class="form-control" name="messages" cols="30" rows="5" placeholder="<?php lang_line('form_contact_comment', true);?>"></textarea>
        </div>
        <button type="submit" class="btn btn3d btn-sky btn-lg call-function" data-callback="callme_callback"><?php lang_line('form_contact_btn', true);?></button>
    </form>
</div>