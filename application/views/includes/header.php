<meta charset="utf-8">
<title><?php if(isset($stitle)) echo $stitle;?></title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="<?php if(isset($skeywords)) echo $skeywords;?>">
<meta name="description" content="<?php if(isset($sdescription)) echo $sdescription;?>">
<meta name="author" content="">

<link rel="shortcut icon" href="<?php echo base_url(); ?>theme/images/ico/favicon.ico">

<link href="<?php echo base_url();?>theme/admin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>theme/admin/bower_components//bootstrap-dialog/bootstrap-dialog.css" rel="stylesheet">
<link href="<?php echo base_url();?>theme/admin/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>theme/admin/css/main.css" rel="stylesheet">
<link href="<?php echo base_url();?>theme/admin/css/sizes.css" rel="stylesheet">
<link href="<?php echo base_url();?>theme/admin/css/animate.css" rel="stylesheet">
<script src="<?php echo base_url();?>theme/admin/bower_components/jquery/dist/jquery.min.js"></script>
<script>
    var base_url = '<?php echo base_url();?>';
</script>