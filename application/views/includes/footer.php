	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-xs-3">					
					<h1 class="text-left">
						<?php lang_title('heading_contacts');?>
					</h1>
					<h4>
						<?php lang_title('heading_address');?>
					</h4>					
					<h2 class="text-left">
						<?php lang_title('heading_phone_booking');?>
					</h2>
					<h4>
						<i class="fa fa-mobile"></i> +373 783 5 35 35
						<br>
						<i class="fa fa-phone"></i> +373 231 9 22 15
					</h4>
				</div>
				<div class="col-xs-9">					
					<h1 class="text-center">
						<i class="fa fa-map-marker"></i>
						<?php lang_title('heading_where_we_are');?>
					</h1>
					<a class="fancybox" href="<?php echo base_url('theme/images/map_big.png');?>">
						<img src="<?php echo base_url('theme/images/map.png');?>" alt="">
					</a>
				</div>
			</div>
		</div>
    </section>

    <footer id="footer">
        <div class="container">
			<div class="row">
				<div class="col-xs-12 lh-36">
					&copy; 2015 <a target="_blank" href="https://plus.google.com/+AndreiCravciuc/posts" title="Cravciuc Andrei - allKaribu">allKaribu</a>. All Rights Reserved.
				</div>
			</div>
        </div>
    </footer><!--/#footer-->
    
	<div class="system-text-messages-b" style="display: none;">	
		<div class="text-b">
			<ul></ul>
		</div>
	</div>
    <script src="<?php echo base_url(); ?>theme/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/bootstrap-dialog.js"></script>
    <script src="<?php echo base_url(); ?>theme/js/main.js"></script>