<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Translations_model extends CI_Model{
	
	var $translations = "translations";
	var $i18n_groups = array(
		array(
			'id_group' => 1,
			'group_name' => 'Шапка сайта',
		),
		array(
			'id_group' => 2,
			'group_name' => 'Меню сайта',
		),
		array(
			'id_group' => 3,
			'group_name' => 'Блок Наши товары',
		),
		array(
			'id_group' => 4,
			'group_name' => 'Блок Как мы работаем',
		),
		array(
			'id_group' => 5,
			'group_name' => 'Блок Окна',
		),
		array(
			'id_group' => 6,
			'group_name' => 'Блок Видео',
		),
		array(
			'id_group' => 7,
			'group_name' => 'Блок Vefasistem в цифрах',
		),
		array(
			'id_group' => 8,
			'group_name' => 'Блок Контакты',
		)
	);
	
	
	function __construct(){
		parent::__construct();
	}
	
	function set_translations($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->insert($this->translations, $data);
		return $this->db->insert_id();
	}
	
	function update_translations($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->update_batch($this->translations, $data, 'translation_key');
	}
	
	function get_translations(){
		return $this->db->get($this->translations)->result_array();
	}
}

?>