<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка: Пройдите авторизацию.');
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Филиалы';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Locations_model", "locations");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$this->data['main_content'] = 'admin/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_location = (int)$this->uri->segment(4);
		$this->data['location'] = $this->locations->handler_get($id_location);
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('city_ru', 'Город RU', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('city_ro', 'Город RO', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('city_en', 'Город EN', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('address_ru', 'Адрес RU', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('address_ro', 'Адрес RO', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('address_en', 'Адрес EN', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('location_latitude', 'Местоположение на карте', 'required|xss_clean|max_length[50]',
					array(
						'required' => 'Выберите местоположение на карте.'
					)
				);
				$this->form_validation->set_rules('location_longitude', 'Местоположение на карте', 'required|xss_clean|max_length[50]',
					array(
						'required' => 'Выберите местоположение на карте.'
					)
				);
				$this->form_validation->set_rules('phone', 'Телефон', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('fax', 'Факс', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|max_length[50]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'location_city_ru' => $this->input->post('city_ru', true),
					'location_city_ro' => $this->input->post('city_ro', true),
					'location_city_en' => $this->input->post('city_en', true),
					'location_address_ru' => $this->input->post('address_ru', true),
					'location_address_ro' => $this->input->post('address_ro', true),
					'location_address_en' => $this->input->post('address_en', true),
					'location_phone' => $this->input->post('phone', true),
					'location_fax' => $this->input->post('fax', true),
					'location_email' => $this->input->post('email', true),
					'location_latitude' => $this->input->post('location_latitude', true),
					'location_longitude' => $this->input->post('location_longitude', true)
				);

				$this->locations->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
                $this->form_validation->set_rules('location', 'Филиал', 'required|xss_clean');
				$this->form_validation->set_rules('city_ru', 'Город RU', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('city_ro', 'Город RO', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('city_en', 'Город EN', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('address_ru', 'Адрес RU', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('address_ro', 'Адрес RO', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('address_en', 'Адрес EN', 'required|xss_clean|max_length[200]');
				$this->form_validation->set_rules('location_latitude', 'Местоположение на карте', 'required|xss_clean|max_length[50]',
					array(
						'required' => 'Выберите местоположение на карте.'
					)
				);
				$this->form_validation->set_rules('location_longitude', 'Местоположение на карте', 'required|xss_clean|max_length[50]',
					array(
						'required' => 'Выберите местоположение на карте.'
					)
				);
				$this->form_validation->set_rules('phone', 'Телефон', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('fax', 'Факс', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|max_length[50]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_location = (int)$this->input->post('location');
				$update = array(
					'location_city_ru' => $this->input->post('city_ru', true),
					'location_city_ro' => $this->input->post('city_ro', true),
					'location_city_en' => $this->input->post('city_en', true),
					'location_address_ru' => $this->input->post('address_ru', true),
					'location_address_ro' => $this->input->post('address_ro', true),
					'location_address_en' => $this->input->post('address_en', true),
					'location_phone' => $this->input->post('phone', true),
					'location_fax' => $this->input->post('fax', true),
					'location_email' => $this->input->post('email', true),
					'location_latitude' => $this->input->post('location_latitude', true),
					'location_longitude' => $this->input->post('location_longitude', true)
				);

				$this->locations->handler_update($id_location, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				$this->form_validation->set_rules('location', 'Филиал', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_location = (int)$this->input->post('location');
				$this->locations->handler_delete($id_location);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_weight':
				$this->form_validation->set_rules('location', 'Филиал', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_location = (int) $this->input->post('location');
				$location = $this->locations->handler_get($id_location);
				if(empty($location)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$params = array(
					'location_weight' => $location['location_weight']
				);

				if($this->input->post('direction') != 'down'){
					$params['direction'] = 'up';
				}

				$last_location = $this->locations->handler_get_weight($params);
				if(!empty($last_location)){
					$location_weight = $last_location['location_weight'];
					$update = array(
						array(
							'id_location' => $id_location,
							'location_weight' => $last_location['location_weight']
						),
						array(
							'id_location' => $last_location['id_location'],
							'location_weight' => $location['location_weight']
						)
					);
					$this->locations->handler_update_locations($update);
				}

				jsonResponse('Сохранено.','success');
			break;
		}
	}

	function ajax_list_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$params = array(
            'limit' => intVal($_POST['iDisplayLength']),
            'start' => intVal($_POST['iDisplayStart'])
        );

        if ($_POST['iSortingCols'] > 0) {
            for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
                switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
                    case 'dt_id': $params['sort_by'][] = 'id_location-' . $_POST['sSortDir_' . $i];
                    break;
                    case 'dt_name': $params['sort_by'][] = 'location_name-' . $_POST['sSortDir_' . $i];
                    break;
                }
            }
        }

        $records = $this->locations->handler_get_all($params);
        $records_total = $this->locations->handler_get_count($params);

        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $records_total,
            "iTotalDisplayRecords" => $records_total,
            "aaData" => array()
        );

        foreach ($records as $record) {
            $output['aaData'][] = array(
                'dt_id'			=>  $record['id_location'],
                'dt_city'		=>  $record['location_city_ru'],
                'dt_address'	=>  $record['location_address_ru'],
                'dt_phone'		=>  $record['location_phone'],
                'dt_fax'		=>  $record['location_fax'],
                'dt_email'		=>  $record['location_email'],
				'dt_actions'	=>  '<span class="btn btn-default btn-xs call-function" title="Поднять на уровень выше" data-callback="change_weight" data-location="'.$record['id_location'].'" data-direction="up"><i class="fa fa-arrow-up"></i></span>
									<span class="btn btn-default btn-xs call-function" title="Отпустить на уровень ниже" data-callback="change_weight" data-location="'.$record['id_location'].'" data-direction="down"><i class="fa fa-arrow-down"></i></span>
									<a href="'.base_url('admin/locations/edit/'.$record['id_location']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
									<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить филиал?" data-callback="delete_action" data-location="'.$record['id_location'].'"><i class="fa fa-remove"></i></a>'
            );
        }
        jsonResponse('', 'success', $output);
	}
}
?>