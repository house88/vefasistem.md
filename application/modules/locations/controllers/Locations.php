<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Locations extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->ulang = $this->data['ulang'] = $this->lang->lang();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];
		$this->load->model("Locations_model", "locations");
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function _get_markers(){
		$records = $this->locations->handler_get_all();
		$markers = array();
		foreach ($records as $record) {
			$markers[] = array(
				'id_location' => $record['id_location'],
				'city' => $record["location_city_{$this->ulang}"],
				'latitude' => $record["location_latitude"],
				'longitude' => $record["location_longitude"],
				'address' => $record["location_address_{$this->ulang}"],
				'phone' => $record["location_phone"],
				'fax' => $record["location_fax"],
				'email' => $record["location_email"],
				'location_weight' => $record['location_weight']
			);
		}
		
		// Сортируем и выводим получившийся массив
		uasort($markers, function ($a, $b){
			if ($a['location_weight'] == $b['location_weight']) {
				return 0;
			}

			return ($a['location_weight'] < $b['location_weight']) ? -1 : 1;
		});
		echo '<pre>';
		print_r($markers);
		echo '</pre>';
		return json_encode($markers, JSON_FORCE_OBJECT);
	}
}
?>

