<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Locations_model extends CI_Model{
	var $locations = "locations";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->locations, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_location, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_location', $id_location);
		$this->db->update($this->locations, $data);
	}

	function handler_delete($id_location){
		$this->db->where('id_location', $id_location);
		$this->db->delete($this->locations);
	}

	function handler_get($id_location){
		$this->db->where('id_location', $id_location);
		return $this->db->get($this->locations)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " location_weight ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($locations_list)){
			$this->db->where_in('id_location', $locations_list);
        }

        if(isset($active)){
			$this->db->where('location_active', $active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->locations)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($locations_list)){
			$this->db->where_in('id_location', $locations_list);
        }

        if(isset($active)){
			$this->db->where('location_active', $active);
        }

		return $this->db->count_all_results($this->locations);
	}

	function handler_get_weight($conditions = array()){
		$direction = 'down';
		extract($conditions);

		if(isset($location_weight)){
			if($direction == 'down'){
				$this->db->where('location_weight', $location_weight + 1);
			} else{
				$this->db->where('location_weight', $location_weight - 1);
			}
		} else{
			$this->db->order_by('location_weight', 'DESC');
		}

		$this->db->limit(1);
		$query = $this->db->get($this->locations);
		
		return $query->row_array();            
	}
	
	function handler_update_locations($data = array(), $column = 'id_location'){
		if(empty($data)){
			return false;
		}

		return $this->db->update_batch($this->locations, $data, $column);
	}
}
?>
