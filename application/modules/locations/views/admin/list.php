<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/locations/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить филиал
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="settings_form">			
			<div class="box-body">
				<div class="directory-search-key-b mt-15"></div>
				<table class="table table-bordered table-striped" id="dtTable">
					<thead>
						<tr>
							<th class="w-50 text-center dt_id">#</th>
							<th class="w-100 text-center dt_city">Город</th>
							<th class="dt_address">Адрес</th>
							<th class="w-100 dt_phone">Телефон</th>
							<th class="w-100 dt_fax">Факс</th>
							<th class="w-100 dt_email">Email</th>
							<th class="w-120 text-center dt_actions">Операций</th>
						</tr>
					</thead>
				</table>
			</div>
			<!-- /.box-body -->
		</form>
	</div>
</div>
<script>
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/locations/ajax_list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_city"], "mData": "dt_city", "bSortable": false},
				{ "sClass": "text-left vam", "aTargets": ["dt_address"], "mData": "dt_address"},
				{ "sClass": "w-100 text-left vam", "aTargets": ["dt_phone"], "mData": "dt_phone"},
				{ "sClass": "w-100 text-left vam", "aTargets": ["dt_fax"], "mData": "dt_fax"},
				{ "sClass": "w-100 text-left vam", "aTargets": ["dt_email"], "mData": "dt_email"},
				{ "sClass": "w-120 text-center vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error')
							systemMessages(data.message, 'message-' + data.mess_type);
						if(data.mess_type == 'info')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_paginate.dataTables_paginate').css("display", "block"); 
                    $('#dtTable_paginate.dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_paginate.dataTables_paginate').css("display", "none");
                    $('#dtTable_paginate.dataTables_filter').css("display", "none");
                }
			}
		});
	});
	
	var delete_action = function(btn){
		var $this = $(btn);
		var location = $this.data('location');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/locations/ajax_operations/delete',
			data: {location:location},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var change_weight = function(btn){
		var $this = $(btn);
		var location = $this.data('location');
		var direction = $this.data('direction');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/locations/ajax_operations/change_weight',
			data: {location:location,direction:direction},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>
