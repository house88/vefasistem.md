<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/locations/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить филиал
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="add_form">			
			<div class="box-body">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">RU</a>
						</li>
						<li class="">
							<a href="#form_ro" data-toggle="tab" aria-expanded="false">RO</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">EN</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ru">							
							<div class="form-group">
								<label>Город</label>
								<input class="form-control" placeholder="Город" name="city_ru" value="" maxlength="50">
								<p class="help-block">Не должно содержать более 50 символов.</p>
							</div>
							<div class="form-group">
								<label>Адрес</label>
								<input class="form-control" placeholder="Адрес" name="address_ru" value="" maxlength="200">
								<p class="help-block">Не должно содержать более 200 символов.</p>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_ro">
							<div class="form-group">
								<label>Город</label>
								<input class="form-control" placeholder="Город" name="city_ro" value="" maxlength="50">
								<p class="help-block">Не должно содержать более 50 символов.</p>
							</div>
							<div class="form-group">
								<label>Адрес</label>
								<input class="form-control" placeholder="Адрес" name="address_ro" value="" maxlength="200">
								<p class="help-block">Не должно содержать более 200 символов.</p>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Город</label>
								<input class="form-control" placeholder="Город" name="city_en" value="" maxlength="50">
								<p class="help-block">Не должно содержать более 50 символов.</p>
							</div>
							<div class="form-group">
								<label>Адрес</label>
								<input class="form-control" placeholder="Адрес" name="address_en" value="" maxlength="200">
								<p class="help-block">Не должно содержать более 200 символов.</p>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
				<div class="form-group">
					<p class="help-block">Выберите местоположение на карте.</p>					
					<div class="map-element" id="mapid"></div>
					<input type="hidden" name="location_latitude">
					<input type="hidden" name="location_longitude">
				</div>
				<div class="form-group">
					<label>Телефон</label>
					<input class="form-control" placeholder="Телефон" name="phone" value="">
				</div>
				<div class="form-group">
					<label>Факс</label>
					<input class="form-control" placeholder="Факс" name="fax" value="">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input class="form-control" placeholder="Email" name="email" value="">
				</div>
			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<script>
	$(function(){
		'use strict';
	});

	var mymap = L.map('mapid', {
		center: [46.99524110694596,28.575439453125004],
		zoom: 7,
		scrollWheelZoom: true
	});
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: '',
		maxZoom: 18,
		id: 'house88.7b4546ed',
		accessToken: 'pk.eyJ1IjoiaG91c2U4OCIsImEiOiJjaXlqeDh1amUwMDB2MndyNHV4NzQxbm0zIn0.bEbZ4H00DAZKEyM18j7Ozg'
	}).addTo(mymap);
	var marker = null;
	mymap.on('click', function(e) {
		if(marker == null){
			marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(mymap);
		} else{
			var latlng = new L.latLng(e.latlng.lat, e.latlng.lng);
			marker.setLatLng(latlng);
		}
		$('input[name="location_latitude"]').val(e.latlng.lat);
		$('input[name="location_longitude"]').val(e.latlng.lng);
	});
	
	var add_form = $('#add_form');
	add_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = add_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/locations/ajax_operations/add',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				if(resp.mess_type == 'success'){
					add_form.replaceWith('<div class="box-body"><div class="alert alert-success mb-0 ml-5 mr-5">'+resp.message+'</div></div>');
				} else{
					systemMessages(resp.message, resp.mess_type);
				}
			}
		});
		return false;
	});
</script>
