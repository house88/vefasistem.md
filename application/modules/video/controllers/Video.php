<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Video extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['lang'] = $this->lang->lang();

		$this->load->model("Video_model", "video");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		show_404();
	}

	function _get_videos($params = array()){
		extract($params);

		return $this->video->handler_get_all();
	}
}
?>
