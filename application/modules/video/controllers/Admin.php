<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Error: Please signin first.');
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Видео';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Video_model", "video");
        $this->load->model("menu/Menu_model", "menu");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/list';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_video = (int)$this->uri->segment(4);
		$this->data['video'] = $this->video->handler_get($id_video);
		$this->data['main_title'] = 'Редактировать видео';
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'edit':
                $this->form_validation->set_rules('video', 'Видео', 'required|xss_clean');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('text_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('text_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('text_en', 'Текст EN', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_video = (int)$this->input->post('video');
				$update = array(
					'video_title_ru' 	=> $this->input->post('title_ru'),
					'video_title_ro' 	=> $this->input->post('title_ro'),
					'video_title_en' 	=> $this->input->post('title_en'),
					'video_text_ru' 	=> $this->input->post('text_ru'),
					'video_text_ro' 	=> $this->input->post('text_ro'),
					'video_text_en' 	=> $this->input->post('text_en')
				);

				$this->video->handler_update($id_video, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'list_dt':		
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);
		
				$records = $this->video->handler_get_all($params);
				$records_total = $this->video->handler_get_count($params);
		
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
		
				foreach ($records as $record) {
					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_video'],
						'dt_name'		=>  $record['video_title_ru'],
						'dt_actions'	=>  '<div class="btn-group">
												<a href="'.base_url('admin/video/edit/'.$record['id_video']).'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
											</div>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
?>
