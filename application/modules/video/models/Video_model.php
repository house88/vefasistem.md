<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Video_model extends CI_Model{
	var $video = "video";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->video, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_video, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_video', $id_video);
		$this->db->update($this->video, $data);
	}

	function handler_delete($id_video){
		$this->db->where('id_video', $id_video);
		$this->db->delete($this->video);
	}

	function handler_get($id_video){
		$this->db->where('id_video', $id_video);
		return $this->db->get($this->video)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_video ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_video)){
			$this->db->where_in('id_video', $id_video);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->video)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_video)){
			$this->db->where_in('id_video', $id_video);
        }

		return $this->db->count_all_results($this->video);
	}
}
?>
