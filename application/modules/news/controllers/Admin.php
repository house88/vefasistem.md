<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Error: Please signin first.');
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Новости';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("News_model", "news");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$this->data['main_content'] = 'admin/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_news = (int)$this->uri->segment(4);
		$this->data['news'] = $this->news->handler_get($id_news);
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('description', 'Текст статьи', 'required');
				$this->form_validation->set_rules('stext', 'Краткое описание', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url', 'URL', 'required|xss_clean');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md', 'Meta description', 'required|xss_clean|max_length[250]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/news/'.$remove_photo);
						@unlink('files/news/thumbs/thumb_200x200_'.$remove_photo);
					}
				}

				$insert = array(
					'news_title' => $this->input->post('title'),
					'news_description' => $this->input->post('description'),
					'news_small_description' => $this->input->post('stext'),
					'url' => $this->input->post('url'),
					'mk' => $this->input->post('mk'),
					'md' => $this->input->post('md'),
					'news_photo' => ($this->input->post('news_photo'))?$this->input->post('news_photo'):''
				);

				$this->news->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
                $this->form_validation->set_rules('news', 'Новость', 'required|xss_clean');
				$this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('description', 'Текст новости', 'required');
				$this->form_validation->set_rules('stext', 'Краткое описание', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('url', 'URL', 'required|xss_clean');
				$this->form_validation->set_rules('mk', 'Meta keywords', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('md', 'Meta description', 'required|xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_news = (int)$this->input->post('news');
				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/news/'.$remove_photo);
						@unlink('files/news/thumbs/thumb_200x200_'.$remove_photo);
					}
				}

				$update = array(
					'news_title' => $this->input->post('title'),
					'news_description' => $this->input->post('description'),
					'news_small_description' => $this->input->post('stext'),
					'url' => $this->input->post('url'),
					'mk' => $this->input->post('mk'),
					'md' => $this->input->post('md'),
					'news_photo' => ($this->input->post('news_photo'))?$this->input->post('news_photo'):''
				);

				$this->news->handler_update($id_news, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				$this->form_validation->set_rules('news', 'Новость', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_news = (int)$this->input->post('news');
				$news = $this->news->handler_get($id_news);
				if(empty($news)){
					jsonResponse('Новость не существует.');
				}

				if(!empty($news['news_photo'])){
					@unlink('files/news/'.$news['news_photo']);
					@unlink('files/news/thumbs/thumb_200x200_'.$news['news_photo']);
				}

				$this->news->handler_delete($id_news);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				$this->form_validation->set_rules('news', 'Новость', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_news = (int)$this->input->post('news');
				$news = $this->news->handler_get($id_news);
				if(empty($news)){
					jsonResponse('Новость не существует.');
				}
				if($news['news_visible']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->news->handler_update($id_news, array('news_visible' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
		}
	}

	function ajax_list_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$params = array(
            'limit' => intVal($_POST['iDisplayLength']),
            'start' => intVal($_POST['iDisplayStart'])
        );

        if ($_POST['iSortingCols'] > 0) {
            for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
                switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
                    case 'dt_id': $params['sort_by'][] = 'id_news-' . $_POST['sSortDir_' . $i];
                    break;
                    case 'dt_name': $params['sort_by'][] = 'news_title-' . $_POST['sSortDir_' . $i];
                    break;
                }
            }
        }

        $records = $this->news->handler_get_all($params);
        $records_total = $this->news->handler_get_count($params);

        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $records_total,
            "iTotalDisplayRecords" => $records_total,
            "aaData" => array()
        );

        foreach ($records as $record) {
			$status = '<a href="#" data-message="Вы уверены что хотите сделать новость активной?" title="Сделать активной" data-title="Изменение статуса" data-callback="change_status" data-news="'.$record['id_news'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
			if($record['news_visible'] == 0){
				$status = '<a href="#" data-message="Вы уверены что хотите сделать новость неактивной?" title="Сделать неактивной" data-title="Изменение статуса" data-callback="change_status" data-news="'.$record['id_news'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
			}

            $output['aaData'][] = array(
                'dt_id'			=>  $record['id_news'],
                'dt_photo'		=>  '<img src="'.base_url(getImage('files/news/'.$record['news_photo'])).'" alt="'.$record['news_title'].'" class="img-thumbnail mw-100 mh-100">',
                'dt_name'		=>  '<a href="'.base_url('news/detail/'.$record['url'].'-'.$record['id_news']).'">'.$record['news_title'].'</a>',
                'dt_url'		=>  'news/detail/'.$record['url'].'-'.$record['id_news'],
				'dt_actions'	=>  $status.' '
									.'<a href="'.base_url('admin/news/edit/'.$record['id_news']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>'
                					.' <a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить новость?" data-callback="delete_action" data-news="'.$record['id_news'].'"><i class="fa fa-remove"></i></a>'
            );
        }
        jsonResponse('', 'success', $output);
	}

	function upload_photo(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $path = 'files/news';
        if(!is_dir($path))
            mkdir($path, 0755, true);

        if(!is_dir($path.'/thumbs'))
            mkdir($path.'/thumbs', 0755, true);

        $config['upload_path'] = FCPATH . $path;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['file_name'] = uniqid();
        $config['min_width']	= '800';
        $config['min_height']	= '640';
        $config['max_size']	= '2000';

        $this->load->library('upload', $config);
		$this->load->library('image_lib');
        if ( ! $this->upload->do_upload()){
			jsonResponse($this->upload->display_errors('',''),'error');
        }
		$data = $this->upload->data();

		$config = array(
			'source_image'      => $data['full_path'],
			'create_thumb'      => true,
			'thumb_marker'      => 'thumb_200x200_',
			'new_image'         => FCPATH . $path . '/thumbs',
			'maintain_ratio'    => true,
			'width'             => 200,
			'height'            => 200
		);

		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		$info = new StdClass;
		$info->filename = $data['file_name'];
		$files[] = $info;
		jsonResponse('', 'success', array("files" => $files));
    }
}
?>

