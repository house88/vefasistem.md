<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class News_model extends CI_Model{
	var $news = "news";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->news, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_news, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_news', $id_news);
		$this->db->update($this->news, $data);
	}

	function handler_delete($id_news){
		$this->db->where('id_news', $id_news);
		$this->db->delete($this->news);
	}

	function handler_get($id_news){
		$this->db->where('id_news', $id_news);
		return $this->db->get($this->news)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_news ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($news_list)){
			$this->db->where_in('id_news', $news_list);
        }

        if(isset($active)){
			$this->db->where('news_active', $active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->news)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($news_list)){
			$this->db->where_in('id_news', $news_list);
        }

        if(isset($active)){
			$this->db->where('news_active', $active);
        }

		return $this->db->count_all_results($this->news);
	}
}
?>
