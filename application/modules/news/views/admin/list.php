<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/news/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить новость
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="settings_form">			
			<div class="box-body">
				<div class="directory-search-key-b mt-15"></div>
				<table class="table table-bordered table-striped" id="dtTable">
					<thead>
						<tr>
							<th class="w-50 text-center dt_id">#</th>
							<th class="w-100 text-center dt_photo">Фото новости</th>
							<th class="dt_name">Название</th>
							<th class="dt_url">URL</th>
							<th class="w-95 text-center dt_actions">Операций</th>
						</tr>
					</thead>
				</table>
			</div>
			<!-- /.box-body -->
		</form>
	</div>
</div>
<script>
	var dtFilter; //obj for filters
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/news/ajax_list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_photo"], "mData": "dt_photo", "bSortable": false},
				{ "sClass": "text-left vam", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "w-200 text-center vam", "aTargets": ["dt_url"], "mData": "dt_url", "bSortable": false},
				{ "sClass": "w-95 text-center vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [[0,'asc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				if(!dtFilter){
					dtFilter = $('.dt_filter').dtFilters({
						'container': '.directory-search-key-b',
						callBack: function(){ dtTable.fnDraw(); }
					});
				}

				aoData = aoData.concat(dtFilter.getDTFilter());
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error')
							systemMessages(data.message, 'message-' + data.mess_type);
						if(data.mess_type == 'info')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {}
		});
	});
	var delete_action = function(btn){
		var $this = $(btn);
		var news = $this.data('news');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/news/ajax_operations/delete',
			data: {news:news},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
	var change_status = function(btn){
		var $this = $(btn);
		var news = $this.data('news');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/news/ajax_operations/change_status',
			data: {news:news},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>
