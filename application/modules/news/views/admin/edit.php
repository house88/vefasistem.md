<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/news/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить новость
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Фото</label>
					<p class="help-block">Ширина: 800px, Высота: 640px</p>
					<div class="clearfix"></div>
					<span class="btn btn-default btn-file pull-left mb-15">
						<i class="fa fa-picture-o"></i>
						Добавить фото <input id="select_photo" type="file" name="userfile">
					</span>
					<div class="clearfix"></div>
					<div id="news_photo" class="files pull-left w-100pr">
					<?php if($news['news_photo'] != ''){?>
						<div class="user-image-thumbnail-wr">
							<div class="user-image-thumb">
								<img class="img-thumbnail" src="<?php echo base_url('files/news/'.$news['news_photo']);?>">
							</div>
							<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $news['news_photo'];?>">
								<span class="glyphicon glyphicon-remove-circle"></span>
							</a>
							<input type="hidden" name="news_photo" value="<?php echo $news['news_photo'];?>">
						</div>
					<?php }?>
					</div>
				</div>
				<div class="form-group">
					<label>Название</label>
					<input class="form-control"
							placeholder="Название"
							name="title"
							id="title"
							value="<?php echo $news['news_title'];?>">
					<input type="hidden" name="url" id="url" value="<?php echo $news['url'];?>">
					<p class="help-block">Название не должно содержать более 100 символов.</p>
				</div>
				<div class="form-group">
					<label>Краткое описание</label>
					<textarea class="form-control" name="stext"><?php echo $news['news_small_description'];?></textarea>
					<p class="help-block">Не должно содержать более 250 символов.</p>
				</div>
				<div class="form-group">
					<label>Текст</label>
					<textarea class="description" name="description"><?php echo $news['news_description'];?></textarea>
				</div>
				<div class="form-group">
					<label>Meta keywords</label>
					<input class="form-control"
							placeholder="Meta keywords"
							name="mk"
							value="<?php echo $news['mk'];?>">
					<p class="help-block">Не должно содержать более 250 символов.</p>
				</div>
				<div class="form-group">
					<label>Meta description</label>
					<input class="form-control"
							placeholder="Meta description"
							name="md"
							value="<?php echo $news['md'];?>">
					<p class="help-block">Не должно содержать более 250 символов.</p>
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<input type="hidden" name="news" value="<?php echo $news['id_news'];?>">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	$(function(){
		'use strict';
		$('#select_photo').fileupload({
			url: base_url+'admin/news/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					$.each(data.result.files, function (index, file) {
						var template = '<div class="user-image-thumbnail-wr">';
						template += '<div class="user-image-thumb"><img class="img-thumbnail" src="'+base_url+'files/news/'+file.filename+'"/></div>';
						template += '<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+file.filename+'"><span class="glyphicon glyphicon-remove-circle"></span></a>';
						template += '<input type="hidden" name="news_photo" value="'+file.filename+'">';
						template += '</div>';

						if($('#news_photo .user-image-thumbnail-wr').length > 0){
							var unused_photo = $('#news_photo .user-image-thumbnail-wr').find('input[name=news_photo]').val();
							$('#news_photo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
						}
						$('#news_photo').html(template);
					});
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');

		$('#title').liTranslit({
			elAlias: $('#url')
		});
	});
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/news/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}
</script>
