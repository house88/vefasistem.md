<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/static_block/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить блок
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Алиас</label>
					<input class="form-control" placeholder="ex. menu_categories" name="alias" value="<?php echo $static_block['block_alias'];?>">
				</div>
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">RU</a>
						</li>
						<li class="">
							<a href="#form_ro" data-toggle="tab" aria-expanded="false">RO</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">EN</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ru">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php echo $static_block['block_title_ru'];?>">
								<p class="help-block">Название не должно содержать более 100 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="text_ru"><?php echo $static_block['block_text_ru'];?></textarea>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_ro">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ro" value="<?php echo $static_block['block_title_ro'];?>">
								<p class="help-block">Название не должно содержать более 100 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="text_ro"><?php echo $static_block['block_text_ro'];?></textarea>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="<?php echo $static_block['block_title_en'];?>">
								<p class="help-block">Название не должно содержать более 100 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="text_en"><?php echo $static_block['block_text_en'];?></textarea>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<input type="hidden" name="block" value="<?php echo $static_block['id_block'];?>">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<script>
	$(function(){
		var edit_form = $('#edit_form');
		edit_form.submit(function () {
			tinyMCE.triggerSave();
			var fdata = edit_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/static_block/ajax_operations/edit',
				data: fdata,
				dataType: 'JSON',
				success: function(resp){
					systemMessages(resp.message, resp.mess_type);
				}
			});
			return false;
		});
	});
</script>
