<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/static_block/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить блок
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="settings_form">			
			<div class="box-body">
				<div class="directory-search-key-b mt-15"></div>
				<table class="table table-bordered table-striped" id="dtTable">
					<thead>
						<tr>
							<th class="w-50 text-center dt_id">#</th>
							<th class="dt_name">Название</th>
							<th class="dt_alias">Алиас</th>
							<th class="w-95 text-center dt_actions">Операций</th>
						</tr>
					</thead>
				</table>
			</div>
			<!-- /.box-body -->
		</form>
	</div>
</div>
<script>
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/static_block/ajax_operations/list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "text-left vam", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "w-200 text-center vam", "aTargets": ["dt_alias"], "mData": "dt_alias", "bSortable": false},
				{ "sClass": "w-95 text-center vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [[0,'asc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type != 'success')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
                }
			}
		});
	});

	var delete_action = function(btn){
		var $this = $(btn);
		var block = $this.data('block');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/static_block/ajax_operations/delete',
			data: {block:block},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var change_status = function(btn){
		var $this = $(btn);
		var block = $this.data('block');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/static_block/ajax_operations/change_status',
			data: {block:block},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>
