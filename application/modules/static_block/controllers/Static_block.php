<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Static_block extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['lang'] = $this->lang->lang();

		$this->load->model("Static_block_model", "static_block");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		show_404();
	}

	function _get_block($params = array()){
		extract($params);

		if(!isset($alias)){
			return;
		}

		return $this->static_block->handler_get_by_alias($alias);
	}

	function _view($params = array()){
		extract($params);

		if(!isset($alias)){
			return;
		}

		$this->data['static_block'] = $this->static_block->handler_get_by_alias($alias);
		return $this->load->view('show_block', $this->data, true);
	}
}
?>
