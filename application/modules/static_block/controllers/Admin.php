<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Error: Please signin first.');
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Статические блоки';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Static_block_model", "static_block");
        $this->load->model("menu/Menu_model", "menu");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		if(!$this->auth_lib->have_right('manage_static_block')){
			redirect('/admin');
		}

		$this->data['main_content'] = 'admin/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		if(!$this->auth_lib->have_right('manage_static_block')){
			redirect('/admin');
		}

		$this->data['main_title'] = 'Добавить блок';
		$this->data['main_content'] = 'admin/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		if(!$this->auth_lib->have_right('manage_static_block')){
			redirect('/admin');
		}

		$id_block = (int)$this->uri->segment(4);
		$this->data['static_block'] = $this->static_block->handler_get($id_block);
		$this->data['main_title'] = 'Редактировать блок';
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				if(!$this->auth_lib->have_right('manage_static_block')){
					jsonResponse('Ошибка: Нет прав!');
				}

				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('text_ru', 'Текст блока RU', 'required');
				$this->form_validation->set_rules('text_ro', 'Текст блока RO', 'required');
				$this->form_validation->set_rules('text_en', 'Текст блока EN', 'required');
				$this->form_validation->set_rules('alias', 'Алиас', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'block_title_ru' 	=> $this->input->post('title_ru'),
					'block_title_ro' 	=> $this->input->post('title_ro'),
					'block_title_en' 	=> $this->input->post('title_en'),
					'block_text_ru' 	=> $this->input->post('text_ru'),
					'block_text_ro' 	=> $this->input->post('text_ro'),
					'block_text_en' 	=> $this->input->post('text_en'),
					'block_alias' 		=> $this->input->post('alias')
				);

				$this->static_block->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
				if(!$this->auth_lib->have_right('manage_static_block')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('block', 'Блок', 'required|xss_clean');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('text_ru', 'Текст блока RU', 'required');
				$this->form_validation->set_rules('text_ro', 'Текст блока RO', 'required');
				$this->form_validation->set_rules('text_en', 'Текст блока EN', 'required');
				$this->form_validation->set_rules('alias', 'Алиас', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_block = (int)$this->input->post('block');
				$update = array(
					'block_title_ru' 	=> $this->input->post('title_ru'),
					'block_title_ro' 	=> $this->input->post('title_ro'),
					'block_title_en' 	=> $this->input->post('title_en'),
					'block_text_ru' 	=> $this->input->post('text_ru'),
					'block_text_ro' 	=> $this->input->post('text_ro'),
					'block_text_en' 	=> $this->input->post('text_en'),
					'block_alias' => $this->input->post('alias')
				);

				$this->static_block->handler_update($id_block, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				if(!$this->auth_lib->have_right('manage_static_block')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('block', 'Блок', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_block = (int)$this->input->post('block');
				$block = $this->static_block->handler_get($id_block);
				if(empty($block)){
					jsonResponse('Блок не существует.');
				}

				$this->static_block->handler_delete($id_block);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				if(!$this->auth_lib->have_right('manage_static_block')){
					jsonResponse('Ошибка: Нет прав!');
				}

                $this->form_validation->set_rules('block', 'Блок', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_block = (int)$this->input->post('block');
				$block = $this->static_block->handler_get($id_block);
				if(empty($block)){
					jsonResponse('Блок не существует.');
				}

				if($block['block_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->static_block->handler_update($id_block, array('block_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
			case 'list_dt':
				if(!$this->auth_lib->have_right('manage_static_block')){
					jsonDTResponse('Ошибка: Нет прав!');
				}
		
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);
		
				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_block-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 'block_title_ru-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}
		
				$records = $this->static_block->handler_get_all($params);
				$records_total = $this->static_block->handler_get_count($params);
		
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
		
				foreach ($records as $record) {
					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_block'],
						'dt_name'		=>  $record['block_title_ru'],
						'dt_alias'		=>  $record['block_alias'],
						'dt_actions'	=>  '<div class="btn-group">
												<a href="'.base_url('admin/static_block/edit/'.$record['id_block']).'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-message="Вы уверены что хотите удалить блок?" data-callback="delete_action" data-block="'.$record['id_block'].'"><i class="fa fa-times"></i></a>
											</div>'
					);
				}
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
?>
