<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Static_block_model extends CI_Model{
	var $static_block = "static_block";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->static_block, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_block, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_block', $id_block);
		$this->db->update($this->static_block, $data);
	}

	function handler_delete($id_block){
		$this->db->where('id_block', $id_block);
		$this->db->delete($this->static_block);
	}

	function handler_get($id_block){
		$this->db->where('id_block', $id_block);
		return $this->db->get($this->static_block)->row_array();
	}

	function handler_get_by_alias($block_alias){
		$this->db->where('block_alias', $block_alias);
		return $this->db->get($this->static_block)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_block ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($block_list)){
			$this->db->where_in('id_block', $block_list);
        }

        if(isset($active)){
			$this->db->where('block_active', $active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->static_block)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($block_list)){
			$this->db->where_in('id_block', $block_list);
        }

        if(isset($active)){
			$this->db->where('block_active', $active);
        }

		return $this->db->count_all_results($this->static_block);
	}
}
?>
