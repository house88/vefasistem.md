<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth_lib
{
	var $password_salt = '';
	public function __construct(){
		$this->ci =& get_instance();
	}    
	
	function logged_in(){
		return (bool) $this->ci->session->userdata('logged_in');
	}
	
	function logout(){
		$this->ci->session->sess_destroy();
	}
	
	function user_data(){
		if($this->logged_in()){
			return $this->ci->auth_model->handler_get($this->ci->session->userdata('id_user'));
		}else{
			return false;
		}
	}
	
	function get_user_data($id = 0){
		if($id){
			return $this->ci->auth_model->handler_get($id);
		}else{
			return false;
		}
	}

	function id_user(){
		if($this->logged_in()){
			return (int) $this->ci->session->userdata('id_user');
		}else{
			return 0;
		}
	}

	function user_group_type(){
		if($this->logged_in()){
			return (int) $this->ci->session->userdata('group_type');
		}else{
			return '';
		}
	}
	
	function is_admin(){
		if(in_array($this->ci->session->userdata('group_type'), array('admin', 'content_menedger', 'moderator'))){
			return true;
		}else{
			return false;
		}
	}

	function have_right($rights){
		$all = true;

		$search_rights = explode(',', $rights);
		foreach($search_rights as $right){
			if(!in_array(trim($right), $this->ci->session->userdata('rights'))){
				$all = false;
			}
		}

		return $all;
	}

	function have_right_or($rights){
		$search_rights = explode(',', $rights);
		foreach($search_rights as $right){
			if(in_array(trim($right), $this->ci->session->userdata('rights'))){
				return true;
				exit();
			}

		}
		return false;
	}

	function hash_password($password = ''){
		return md5($password.$this->password_salt);
	}
}
