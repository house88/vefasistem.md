<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		
        $this->data = array();
        $this->breadcrumbs = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}
	
	function index(){
		show_404();
	}
	
	function signin(){
		if($this->auth_lib->logged_in()){
			if($this->auth_lib->is_admin()){
				redirect('admin');
			} else{
				redirect('account');
			}
		}

		$this->data['stitle'] = 'Авторизация';

        $this->breadcrumbs[] = array(
			'title' => 'Авторизация',
			'link' => base_url('auth/signin')
		);

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'auth/signin_view';
		$this->load->view('auth/page_template', $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'signin':
				if($this->auth_lib->logged_in()){
					jsonResponse('Вы уже авторизированы.', 'info');
				}
				
				$config = array(
					array(
						'field' => 'user_email',
						'label' => 'E-mail',
						'rules' => 'required|xss_clean|valid_email',
					),
					array(
						'field' => 'password',
						'label' => 'Password',
						'rules' => 'required|xss_clean',
					)
				);

				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$user_email = $this->input->post('user_email', true);
				$password 	= $this->auth_lib->hash_password($this->input->post('password', true));
				$user_data 	= $this->auth_model->handler_get_login_info($user_email, $password);
				if($user_data){
					if($user_data->status == 0){
						jsonResponse('Учетная запись не активна.');
					}

					if($user_data->user_banned == 1){
						jsonResponse('Учетная запись заблокирована.');
					}

					$session_data['id_user'] = $user_data->id;
					$session_data['group_type'] = $user_data->group_type;
					$session_data['logged_in'] = true;
					$session_data['rights'] = $user_data->user_rights;
					$this->session->set_userdata($session_data);
					if($this->auth_lib->is_admin()){
						$redirect = 'admin';
					} else{
						$redirect = 'account';
					}

					jsonResponse('', 'success', array('redirect' => $redirect));
				} else{
					jsonResponse('Данные для входа неверны.');
				}
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
	
	function logout(){
		$this->auth_lib->logout();
		redirect('');
	}
}