<div class="static-form-container mb-15">
    <div class="collapsed in">
        <h1>Регистрация</h1><br>
        <form id="static_register_form">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="ca-icon ca-icon_user"></span>
                    </div>
                    <input type="text"name="username" class="form-control mb-0" placeholder="Имя пользователя">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="ca-icon ca-icon_envelope"></span>
                    </div>
                    <input type="text" name="email" class="form-control mb-0" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span class="ca-icon ca-icon_password"></span>
                    </div>
                    <input type="password" name="password" class="form-control mb-0" placeholder="Пароль">
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-danger btn-lg btn-block mb-15">Регистрация</button>
            </div>
            
            <div class="wr-help">
                <a href="<?php echo base_url('signin');?>" class="pull-left">Авторизация</a>
                <a href="<?php echo base_url('forgot_password');?>" class="pull-right">Востановить пароль</a>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $('#static_register_form').on('submit', function(e){
            var $form = $(this);
            var fdata = $form.serialize();

            $.ajax({
				type: 'POST',
				url: base_url+'auth/ajax_operations/signup',
				data: fdata,
				dataType: 'JSON',
                beforeSend: function(){
                    showLoader('body');
                },
				success: function(resp){
                    systemMessages(resp.message, resp.mess_type);
					hideLoader('body');
                    if(resp.mess_type == 'success'){
                        $form[0].reset();
					}
				}
			});
            return false;
        });
    });
</script>