<div class="auth-logo">
    <span>Авторизация</span>
</div>
<form id="static_signin_form">
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
            <input type="text" name="user_email" class="form-control" placeholder="Email">
        </div>
    </div>
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
            <input type="password" name="password" class="form-control mb-0" placeholder="Пароль">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 text-center">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Войти</button>
        </div>
    </div>
</form>
<script>
    $(function(){
        $('#static_signin_form').on('submit', function(e){
            var $form = $(this);
            var fdata = $form.serialize();

            $.ajax({
				type: 'POST',
				url: base_url+'auth/ajax_operations/signin',
				data: fdata,
				dataType: 'JSON',
                beforeSend: function(){
                    clearSystemMessages();
                },
				success: function(resp){
					if(resp.mess_type == 'success'){
                        window.location.href = base_url + resp.redirect;
					} else{
						systemMessages(resp.message, resp.mess_type);;
					}
				}
			});
            return false;
        });
    });
</script>