<!DOCTYPE html>
<html>
<head>
    <?php $this->load->view('admin/includes/header');?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <?php $this->load->view('admin/includes/navigation');?>
        <div class="content-wrapper">
            <section class="content">
                <div class="row">
                    <?php $this->load->view($main_content);?>
                </div>
            </section>
        </div>
        <?php $this->load->view('admin/includes/footer');?>
    </div>
</body>
</html>
