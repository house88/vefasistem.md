<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="settings_form">			
			<div class="box-body no-padding">
				<table class="table">
					<thead>
						<tr>
							<th>Название</th>
							<th>Значение</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($settings as $setting){?>
							<tr>
								<td class="w-200">
									<strong><?php echo $setting['setting_title'];?></strong>
								</td>
								<td>
									<input type="text" name="settings[<?php echo $setting['setting_alias'];?>]" class="form-control" value="<?php echo $setting['setting_value'];?>">
								</td>
							</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	var settings_form = $('#settings_form');
	settings_form.submit(function () {
		var fdata = settings_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/save_settings',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});
</script>