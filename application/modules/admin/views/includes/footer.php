<div class="system-text-messages-b" style="<?php if(empty($system_messages)){echo 'display: none;';}?>">	
	<div class="text-b">
		<ul>
			<?php if(!empty($system_messages)){echo $system_messages;}?>
		</ul>
	</div>
</div>

<footer class="main-footer">
	<strong>Copyright &copy; 2017-<?php echo date('Y');?>.</strong> All rights reserved.
</footer>