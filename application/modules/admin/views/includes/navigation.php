<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>admin" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="<?php echo base_url();?>">
                                <i class="fa fa-home"></i> Перейти на сайт
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('admin/edit');?>">
                                <i class="fa fa-cogs"></i> Личные данные
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('logout');?>">
                                <i class="fa fa-sign-out"></i> Выход
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
            </ul>        
        </div>
    </nav>
</header>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" id="side-menu">
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Панель управления</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url();?>admin/settings"><i class="fa fa-cogs"></i> <span>Настройки</span></a></li>
                    <li><a href="<?php echo base_url();?>admin/translations"><i class="fa fa-cogs"></i> <span>Перевод</span></a></li>
                </ul>
            </li>
            <!-- <li><a href="<?php echo base_url();?>admin/news"><i class="fa fa-newspaper-o"></i> <span>Новости</span></a></li> -->
            <li><a href="<?php echo base_url();?>admin/locations"><i class="fa fa-map"></i> <span>Филиалы</span></a></li>
            <li><a href="<?php echo base_url();?>admin/products"><i class="fa fa-list"></i> <span>Блок товары</span></a></li>
            <li><a href="<?php echo base_url();?>admin/windows"><i class="fa fa-list"></i> <span>Блок окна</span></a></li>
            <li><a href="<?php echo base_url();?>admin/static_block"><i class="fa fa-list-alt"></i> <span>Статические блоки</span></a></li>
            <li><a href="<?php echo base_url();?>admin/video"><i class="fa fa-video-camera"></i> <span>Видео Trocal 76</span></a></li>
            <li><a href="<?php echo base_url();?>admin/contacts"><i class="fa fa-comments"></i> <span>Обратная связь</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>