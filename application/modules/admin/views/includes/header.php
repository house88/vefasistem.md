<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Панель управления</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="<?php echo base_url();?>theme/admin/plugins/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url();?>theme/admin/fonts/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="<?php echo base_url();?>theme/admin/fonts/ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?php echo base_url();?>theme/admin/css/AdminLTE.css">
<link rel="stylesheet" href="<?php echo base_url();?>theme/admin/css/style.css">
<link rel="stylesheet" href="<?php echo base_url();?>theme/admin/css/sizes.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="<?php echo base_url();?>theme/admin/css/skins/skin-blue.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url();?>theme/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>theme/admin/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- FastClick -->
<script src="<?php echo base_url();?>theme/admin/plugins/fastclick/fastclick.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url();?>theme/admin/js/app.min.js"></script>

<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url();?>theme/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    
<!-- DATATABLE -->
<link href="<?php echo base_url();?>theme/admin/plugins/datatable/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>theme/admin/plugins/datatable/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>theme/admin/plugins/datatable/js/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>theme/admin/plugins/datatable/js/jquery.dtFilters.js"></script>

<!-- Bootstrap Dialog -->
<link href="<?php echo base_url();?>theme/admin/plugins/bootstrap-dialog/bootstrap-dialog.css" rel="stylesheet">
<script src="<?php echo base_url();?>theme/admin/plugins/bootstrap-dialog/bootstrap-dialog.js"></script>

<!-- Fileupload -->
<script src="<?php echo base_url();?>theme/admin/plugins/fileupload/jquery.ui.widget.js"></script>
<script src="<?php echo base_url();?>theme/admin/plugins/fileupload/jquery.fileupload.js"></script>

<!-- liTranslit -->
<script src="<?php echo base_url();?>theme/admin/plugins/litranslit/jquery.liTranslit.js"></script>

<!-- TOOLTIPSTER -->
<link href="<?php echo base_url();?>theme/admin/plugins/tooltipster/tooltipster.bundle.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>theme/admin/plugins/tooltipster/tooltipster.bundle.js"></script>

<!-- CLIPBOARD -->
<script src="<?php echo base_url(); ?>theme/admin/plugins/clipboard/clipboard.js"></script>

<!-- iCheck -->
<link href="<?php echo base_url();?>theme/admin/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url(); ?>theme/admin/plugins/icheck/icheck.js"></script>


<link rel="stylesheet" href="<?php echo base_url();?>theme/admin/plugins/leaflet/leaflet.css" />
<script src="<?php echo base_url();?>theme/admin/plugins/leaflet/leaflet.js"></script>

<script src="<?php echo base_url();?>theme/admin/js/scripts.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>theme/admin/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    var base_url = '<?php echo base_url();?>';

    tinymce.init({
        relative_urls: false,
        selector: ".description",
        height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor responsivefilemanager"
        ],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | fontsizeselect | styleselect",
        toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview media fullpage ",
        image_advtab: true ,
        fontsize_formats: '8px 10px 12px 14px 16px 18px 20px 22px 24px 36px',
        external_filemanager_path:"/theme/admin/plugins/filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        external_plugins: { "filemanager" : "/theme/admin/plugins/filemanager/plugin.min.js"}
    });
    
    $(function(){
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        }).on('ifClicked ifChanged ifChecked ifUnchecked check', function(event){                
            $(this).trigger('click');  
            $('input').iCheck('update');
        });
    });
</script>