<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
                <div class="form-group">
                    <label>Имя</label>
                    <input class="form-control" type="text" placeholder="Имя" name="user_name" value="<?php echo $user->user_name;?>">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="text" placeholder="Email" name="user_email" value="<?php echo $user->user_email;?>">
                </div>
                <div class="form-group">
                    <label>Новый пароль</label>
                    <input class="form-control" type="password" placeholder="Новый пароль" name="user_password" value="">
                </div>
                <div class="form-group">
                    <label>Повторите новый пароль</label>
                    <input class="form-control" type="password" placeholder="Повторите новый пароль" name="confirm_password" value="">
                </div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>

<script>
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});
</script>