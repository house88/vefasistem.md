<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings_model extends CI_Model{
	
	var $settings = "settings";
	var $settings_home = "settings_home";
	
	function __construct(){
		parent::__construct();
	}
	
	function set_settings($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->insert($this->settings, $data);
		return $this->db->insert_id();
	}
	
	function update_settings($data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->update_batch($this->settings, $data, 'setting_alias');
	}
	
	function get_settings(){
		$this->db->order_by('id', 'asc');
		return $this->db->get($this->settings)->result_array();
	}
	
	function update_setting_home($setting_alias, $data = array()){
		if(empty($data)){
			return;
		}
		
		$this->db->where('setting_alias', $setting_alias);
		$this->db->update($this->settings_home, $data);
	}
	
	function get_settings_home(){
		$this->db->order_by('id_setting', 'asc');
		return $this->db->get($this->settings_home)->result_array();
	}
}

?>