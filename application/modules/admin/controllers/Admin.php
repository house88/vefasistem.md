<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
		
		if(!$this->auth_lib->logged_in()){
			if (!$this->input->is_ajax_request()) {
				redirect('signin');
			} else{
				jsonResponse('Ошибка: Пройдите авторизацию.');
			}
		}
		
		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Ошибка: Нет прав.');
			}
		}

		$this->data = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}
	
	function index(){
		$this->settings();
	}
	
	function settings(){
		$this->data['main_title'] = 'Настройки';
		$this->data['main_content'] = 'admin/settings_view';
		$this->load->view('admin/page', $this->data);
	}
	
	function edit(){
		$this->data['user'] = $this->auth_lib->user_data();
		$this->data['main_title'] = 'Личные данные';
		$this->data['main_content'] = 'admin/edit_form_view';
		$this->load->view('admin/page', $this->data);
	}
	
	function translations(){
		$this->load->model('translations/Translations_model', 'translations');
		$this->data['translations'] = $this->translations->get_translations();
		$this->data['main_title'] = 'Переводы';
		$this->data['main_content'] = 'translations/translations_view';
		$this->load->view('admin/page', $this->data);
	}
	
	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(3);
		switch($option){
			case 'save_settings':
                $this->form_validation->set_rules('settings[]', 'Настройки', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }
				
				$settings = $this->input->post('settings');
				$update = array();
				foreach($settings as $key => $setting){
					$update[] = array(
						'setting_alias' => $key,
						'setting_value' => $setting
					);
				}
				
				$this->settings->update_settings($update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'save_translations':
				$translations = $this->input->post('translations');
				$update = array();
				foreach($translations as $key => $translation){
					$update[] = array(
						'translation_key' => $key,
						'translation_text_ro' => $translation['ro'],
						'translation_text_ru' => $translation['ru'],
						'translation_text_en' => $translation['en']
					);
				}
				
				$this->load->model('translations/Translations_model', 'translations');
				$this->translations->update_translations($update);

				$lang_file = APPPATH . 'language/romanian/site_lang.php';
				$f = fopen($lang_file, "w");
				fwrite($f, '<?php '."\r\n");
				foreach($update as $lang_record){
					fwrite($f, '$lang[\''.$lang_record['translation_key'].'\'] = \''.$lang_record['translation_text_ro'].'\';'."\r\n");
				}
				fclose($f);

				$lang_file = APPPATH . 'language/russian/site_lang.php';
				$f = fopen($lang_file, "w");
				fwrite($f, '<?php '."\r\n");
				foreach($update as $lang_record){
					fwrite($f, '$lang[\''.$lang_record['translation_key'].'\'] = \''.$lang_record['translation_text_ru'].'\';'."\r\n");
				}
				fclose($f);
				
				$lang_file = APPPATH . 'language/english/site_lang.php';
				$f = fopen($lang_file, "w");
				fwrite($f, '<?php '."\r\n");
				foreach($update as $lang_record){
					fwrite($f, '$lang[\''.$lang_record['translation_key'].'\'] = \''.$lang_record['translation_text_en'].'\';'."\r\n");
				}
				fclose($f);
				
				jsonResponse('Сохранено.', 'success');
			break;			
			case 'edit':
				$this->form_validation->set_rules('user_name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_email', 'Email', 'required|xss_clean|valid_email');
				$this->form_validation->set_rules('user_password', 'Новый пароль', 'min_length[6]|max_length[50]');
				$password = $this->input->post('user_password');
				if(!empty($password)){
					$this->form_validation->set_rules('confirm_password', 'Повторите новый пароль', 'required|matches[user_password]');
				}

				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$user_info = $this->auth_lib->user_data();
				if(empty($user_info)){
					jsonResponse('Ошибка: Данные не верны.');
				}

				$update = array(
					'user_name' => $this->input->post('user_name', true),
					'user_email' => $this->input->post('user_email', true)
				);

				$password = $this->input->post('user_password');
				if(!empty($password)){
					$update['user_pass'] = $this->auth_lib->hash_password($password);
				}

				$this->auth_model->handler_update($this->auth_lib->id_user(), $update);
				jsonResponse('Сохранено.','success');
			break;
		}
	}
}