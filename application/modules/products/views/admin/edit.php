<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/products/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить товар
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Фото</label>
					<p class="help-block">Ширина: 256px, Высота: 256px</p>
					<div class="clearfix"></div>
					<span class="btn btn-default btn-sm btn-file pull-left mb-15">
						<i class="fa fa-picture-o"></i>
						Добавить фото <input id="select_photo" type="file" name="userfile">
					</span>
					<div class="clearfix"></div>
					<div id="product_photo" class="files pull-left w-100pr">
						<?php if($product['product_photo'] != ''){?>
							<div class="user-image-thumbnail-wr">
								<div class="user-image-thumb">
									<img class="img-thumbnail" src="<?php echo base_url('files/products/'.$product['product_photo']);?>">
								</div>
								<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="<?php echo $product['product_photo'];?>">
									<span class="glyphicon glyphicon-remove-circle"></span>
								</a>
								<input type="hidden" name="product_photo" value="<?php echo $product['product_photo'];?>">
							</div>
						<?php }?>
					</div>
				</div>
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#form_ru" data-toggle="tab" aria-expanded="true">RU</a>
						</li>
						<li class="">
							<a href="#form_ro" data-toggle="tab" aria-expanded="false">RO</a>
						</li>
						<li class="">
							<a href="#form_en" data-toggle="tab" aria-expanded="false">EN</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="form_ru">							
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ru" value="<?php echo $product['product_title_ru'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="text_ru"><?php echo $product['product_text_ru'];?></textarea>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_ro">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_ro" value="<?php echo $product['product_title_ro'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="text_ro"><?php echo $product['product_text_ro'];?></textarea>
							</div>
						</div>
						<!-- /.tab-pane -->
						<div class="tab-pane" id="form_en">
							<div class="form-group">
								<label>Название</label>
								<input class="form-control" placeholder="Название" name="title_en" value="<?php echo $product['product_title_en'];?>">
								<p class="help-block">Название не должно содержать более 250 символов.</p>
							</div>
							<div class="form-group">
								<label>Текст</label>
								<textarea class="description" name="text_en"><?php echo $product['product_text_en'];?></textarea>
							</div>
						</div>
					</div>
					<!-- /.tab-content -->
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<input type="hidden" name="product" value="<?php echo $product['id_product'];?>">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<script>
	$(function(){
		'use strict';
		$('#select_photo').fileupload({
			url: base_url+'admin/products/ajax_operations/upload_photo',
			dataType: 'json',
			done: function (e, data) {
				if(data.result.mess_type == 'error'){
					systemMessages( data.result.message, data.result.mess_class );
				} else{
					$.each(data.result.files, function (index, file) {
						var template = '<div class="user-image-thumbnail-wr">';
						template += '<div class="user-image-thumb"><img class="img-thumbnail" src="'+base_url+'files/products/'+file.filename+'"/></div>';
						template += '<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-callback="remove_photo" data-message="Вы уверены что хотите удалить эту картинку?" title="Удалить" data-title="Удалить" data-photo="'+file.filename+'"><span class="glyphicon glyphicon-remove-circle"></span></a>';
						template += '<input type="hidden" name="product_photo" value="'+file.filename+'">';
						template += '</div>';

						if($('#product_photo .user-image-thumbnail-wr').length > 0){
							var unused_photo = $('#product_photo .user-image-thumbnail-wr').find('input[name="product_photo"]').val();
							$('#product_photo').closest('form').append('<input type="hidden" name="remove_photos[]" value="'+unused_photo+'"/>');
						}
						$('#product_photo').html(template);
					});
				}
			}
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
	
	var edit_form = $('#edit_form');
	edit_form.submit(function () {
		tinyMCE.triggerSave();
		var fdata = edit_form.serialize();
		$.ajax({
			type: 'POST',
			url: base_url+'admin/products/ajax_operations/edit',
			data: fdata,
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
			}
		});
		return false;
	});

	var remove_photo = function(btn){
		var $this = $(btn);
		var photo = $this.data('photo');
		$this.closest('form').append('<input type="hidden" name="remove_photos[]" value="'+photo+'"/>');
		$this.closest('.user-image-thumbnail-wr').remove();
	}
</script>
