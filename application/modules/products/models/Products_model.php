<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends CI_Model{
	var $products = "products";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->products, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_product = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_product', $id_product);
		$this->db->update($this->products, $data);
	}

	function handler_delete($id_product = 0){
		$this->db->where('id_product', $id_product);
		$this->db->delete($this->products);
	}

	function handler_get($id_product = 0){
		$this->db->where('id_product', $id_product);
		return $this->db->get($this->products)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_product ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_product)){
			$this->db->where_in('id_product', $id_product);
        }

        if(isset($product_active)){
			$this->db->where('product_active', $product_active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->products)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_product)){
			$this->db->where_in('id_product', $id_product);
        }

        if(isset($product_active)){
			$this->db->where('product_active', $product_active);
        }

		return $this->db->count_all_results($this->products);
	}
}
?>
