<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Error: Please signin first.');
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Товары';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Products_model", "products");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$this->data['main_content'] = 'admin/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_product = (int)$this->uri->segment(4);
		$this->data['product'] = $this->products->handler_get($id_product);
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('text_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('text_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('text_EN', 'Текст EN', 'required');
				$this->form_validation->set_rules('product_photo', 'Фото', 'required',
					array(
						'required' => 'Фото не загружено.'
					)
				);
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/products/'.$remove_photo);
					}
				}

				$insert = array(
					'product_title_ru' 	=> $this->input->post('title_ru'),
					'product_title_ro' 	=> $this->input->post('title_ro'),
					'product_title_en' 	=> $this->input->post('title_en'),
					'product_text_ru' 	=> $this->input->post('text_ru'),
					'product_text_ro' 	=> $this->input->post('text_ro'),
					'product_text_en' 	=> $this->input->post('text_en'),
					'product_photo' 	=> $this->input->post('product_photo')
				);

				$this->products->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
                $this->form_validation->set_rules('product', 'Товар', 'required|xss_clean');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('text_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('text_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('text_en', 'Текст EN', 'required');
				$this->form_validation->set_rules('product_photo', 'Фото', 'required',
					array(
						'required' => 'Фото не загружено.'
					)
				);

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_product = (int)$this->input->post('product');
				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/products/'.$remove_photo);
					}
				}

				$update = array(
					'product_title_ru' 	=> $this->input->post('title_ru'),
					'product_title_ro' 	=> $this->input->post('title_ro'),
					'product_title_en' 	=> $this->input->post('title_en'),
					'product_text_ru' 	=> $this->input->post('text_ru'),
					'product_text_ro' 	=> $this->input->post('text_ro'),
					'product_text_en' 	=> $this->input->post('text_en'),
					'product_photo' 	=> $this->input->post('product_photo')
				);

				$this->products->handler_update($id_product, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				$this->form_validation->set_rules('product', 'Товар', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_product = (int)$this->input->post('product');
				$product = $this->products->handler_get($id_product);
				if(empty($product)){
					jsonResponse('Товар не существует.');
				}

				@unlink('files/products/'.$product['product_photo']);

				$this->products->handler_delete($id_product);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				$this->form_validation->set_rules('product', 'Товар', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_product = (int)$this->input->post('product');
				$product = $this->products->handler_get($id_product);
				if(empty($product)){
					jsonResponse('Товар не существует.');
				}

				if($product['product_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->products->handler_update($id_product, array('product_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);
		
				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_product-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 'product_title_ru-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}
		
				$records = $this->products->handler_get_all($params);
				$records_total = $this->products->handler_get_count($params);
		
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
		
				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать товар активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-product="'.$record['id_product'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['product_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать товар неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-product="'.$record['id_product'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}
		
					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_product'],
						'dt_photo'		=>  '<img src="'.base_url(getImage('files/products/'.$record['product_photo'])).'" class="img-thumbnail mw-50 mh-50">',
						'dt_name'		=>  $record['product_title_ru'],
						'dt_actions'	=>  '<div class="btn-group">
												'.$status.'
												<a href="'.base_url('admin/products/edit/'.$record['id_product']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить товар?" data-callback="delete_action" data-product="'.$record['id_product'].'"><i class="fa fa-remove"></i></a>
											</div>
											'
						
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'upload_photo':
				$path = 'files/products';
				if(!is_dir($path))
					mkdir($path, 0755, true);
		
				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['max_size']	= '2000';
				
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$upload_data = $this->upload->data();
		
				$info = new StdClass;
				$info->filename = $upload_data['file_name'];
				$files[] = $info;
				jsonResponse('', 'success', array("files" => $files));
			break;
		}
	}
}
?>