<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['lang'] = $this->lang->lang();

		$this->load->model("Products_model", "products");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		show_404();
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		 }
 
		 $action = $this->uri->segment(4);
		 switch ($action) {
			case 'show_product':
				$id_product = (int) $this->input->get('product');
				$this->data['product'] = $this->products->handler_get($id_product);
				$content = $this->load->view('products/popup_product_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		 }
	}

	function _get_all($params = array()){
		return $this->products->handler_get_all($params);
	}
}
?>
