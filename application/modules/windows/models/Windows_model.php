<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Windows_model extends CI_Model{
	var $windows = "windows";
	function __construct(){
		parent::__construct();
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->windows, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_window = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_window', $id_window);
		$this->db->update($this->windows, $data);
	}

	function handler_delete($id_window = 0){
		$this->db->where('id_window', $id_window);
		$this->db->delete($this->windows);
	}

	function handler_get($id_window = 0){
		$this->db->where('id_window', $id_window);
		return $this->db->get($this->windows)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_window ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_window)){
			$this->db->where_in('id_window', $id_window);
        }

        if(isset($window_active)){
			$this->db->where('window_active', $window_active);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->windows)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_window)){
			$this->db->where_in('id_window', $id_window);
        }

        if(isset($window_active)){
			$this->db->where('window_active', $window_active);
        }

		return $this->db->count_all_results($this->windows);
	}
}
?>
