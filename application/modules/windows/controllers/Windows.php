<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Windows extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->load->model("Windows_model", "windows");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		show_404();
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		 }
 
		 $action = $this->uri->segment(4);
		 switch ($action) {
			case 'show_window':
				$id_window = (int) $this->input->get('window');
				$this->data['window'] = $this->windows->handler_get($id_window);
				$content = $this->load->view('windows/popup_window_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		 }
	}

	function _get_windows(){
		$params = array(
			'window_active' => 1
		);

		return $this->windows->handler_get_all($params);
	}
}
?>

