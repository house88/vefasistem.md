<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Error: Please signin first.');
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Окна';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Windows_model", "windows");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/list';
		$this->load->view('admin/page', $this->data);
	}

	function add(){
		$this->data['main_title'] = 'Добавить окно';
		$this->data['main_content'] = 'admin/add';
		$this->load->view('admin/page', $this->data);
	}

	function edit(){
		$id_window = (int)$this->uri->segment(4);
		$this->data['window'] = $this->windows->handler_get($id_window);
		$this->data['main_title'] = 'Редактировать окно';
		$this->data['main_content'] = 'admin/edit';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'add':
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext_ru', 'Краткое описание RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext_ro', 'Краткое описание RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext_en', 'Краткое описание EN', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('text_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('text_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('text_en', 'Текст EN', 'required');
				$this->form_validation->set_rules('window_photo', 'Фото', 'required',
					array(
						'required' => 'Фото не загружено.'
					)
				);
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/windows/'.$remove_photo);
					}
				}

				$insert = array(
					'window_title_ru' 	=> $this->input->post('title_ru'),
					'window_title_ro' 	=> $this->input->post('title_ro'),
					'window_title_en' 	=> $this->input->post('title_en'),
					'window_stext_ru' 	=> nl2br($this->input->post('stext_ru')),
					'window_stext_ro' 	=> nl2br($this->input->post('stext_ro')),
					'window_stext_en' 	=> nl2br($this->input->post('stext_en')),
					'window_text_ru' 	=> $this->input->post('text_ru'),
					'window_text_ro' 	=> $this->input->post('text_ro'),
					'window_text_en' 	=> $this->input->post('text_en'),
					'window_photo' 		=> $this->input->post('window_photo')
				);

				$this->windows->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'edit':
                $this->form_validation->set_rules('window', 'Товар', 'required|xss_clean');
				$this->form_validation->set_rules('title_ru', 'Название RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_ro', 'Название RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('title_en', 'Название EN', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext_ru', 'Краткое описание RU', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext_ro', 'Краткое описание RO', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('stext_en', 'Краткое описание EN', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('text_ru', 'Текст RU', 'required');
				$this->form_validation->set_rules('text_ro', 'Текст RO', 'required');
				$this->form_validation->set_rules('text_en', 'Текст EN', 'required');
				$this->form_validation->set_rules('window_photo', 'Фото', 'required',
					array(
						'required' => 'Фото не загружено.'
					)
				);

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_window = (int)$this->input->post('window');
				$remove_photos = $this->input->post('remove_photos');
				if(!empty($remove_photos)){
					foreach($remove_photos as $remove_photo){
						@unlink('files/windows/'.$remove_photo);
					}
				}

				$update = array(
					'window_title_ru' 	=> $this->input->post('title_ru'),
					'window_title_ro' 	=> $this->input->post('title_ro'),
					'window_title_en' 	=> $this->input->post('title_en'),
					'window_stext_ru' 	=> nl2br($this->input->post('stext_ru')),
					'window_stext_ro' 	=> nl2br($this->input->post('stext_ro')),
					'window_stext_en' 	=> nl2br($this->input->post('stext_en')),
					'window_text_ru' 	=> $this->input->post('text_ru'),
					'window_text_ro' 	=> $this->input->post('text_ro'),
					'window_text_en' 	=> $this->input->post('text_en'),
					'window_photo' 		=> $this->input->post('window_photo')
				);

				$this->windows->handler_update($id_window, $update);
				jsonResponse('Сохранено.', 'success');
			break;
			case 'delete':
				$this->form_validation->set_rules('window', 'Окно', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_window = (int)$this->input->post('window');
				$window = $this->windows->handler_get($id_window);
				if(empty($window)){
					jsonResponse('Окно не существует.');
				}

				@unlink('files/windows/'.$window['window_photo']);

				$this->windows->handler_delete($id_window);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change_status':
				$this->form_validation->set_rules('window', 'Окно', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_window = (int)$this->input->post('window');
				$window = $this->windows->handler_get($id_window);
				if(empty($window)){
					jsonResponse('Окно не существует.');
				}

				if($window['window_active']){
					$status = 0;
				} else{
					$status = 1;
				}
				$this->windows->handler_update($id_window, array('window_active' => $status));
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);
		
				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_window-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_name': $params['sort_by'][] = 'window_title_ru-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}
		
				$records = $this->windows->handler_get_all($params);
				$records_total = $this->windows->handler_get_count($params);
		
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
		
				foreach ($records as $record) {
					$status = '<a href="#" data-message="Вы уверены что хотите сделать окно активным?" title="Сделать активным" data-title="Изменение статуса" data-callback="change_status" data-window="'.$record['id_window'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
					if($record['window_active'] == 0){
						$status = '<a href="#" data-message="Вы уверены что хотите сделать окно неактивным?" title="Сделать неактивным" data-title="Изменение статуса" data-callback="change_status" data-window="'.$record['id_window'].'" class="btn btn-default btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
					}
		
					$output['aaData'][] = array(
						'dt_id'			=>  $record['id_window'],
						'dt_photo'		=>  '<img src="'.base_url(getImage('files/windows/'.$record['window_photo'])).'" class="img-thumbnail mw-50 mh-50">',
						'dt_name'		=>  $record['window_title_ru'],
						'dt_actions'	=>  '<div class="btn-group">
												'.$status.'
												<a href="'.base_url('admin/windows/edit/'.$record['id_window']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
												<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить окно?" data-callback="delete_action" data-window="'.$record['id_window'].'"><i class="fa fa-remove"></i></a>
											</div>
											'
						
					);
				}
				jsonResponse('', 'success', $output);
			break;
			case 'upload_photo':
				$path = 'files/windows';
				if(!is_dir($path))
					mkdir($path, 0755, true);
		
				$config['upload_path'] = FCPATH . $path;
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = uniqid();
				$config['max_size']	= '2000';
				
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload()){
					jsonResponse($this->upload->display_errors('',''),'error');
				}

				$upload_data = $this->upload->data();
		
				$info = new StdClass;
				$info->filename = $upload_data['file_name'];
				$files[] = $info;
				jsonResponse('', 'success', array("files" => $files));
			break;
		}
	}
}
?>