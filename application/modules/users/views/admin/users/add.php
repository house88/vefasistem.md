<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/users/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить пользователя
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="add_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Имя</label>
					<input class="form-control" placeholder="Имя" name="user_name" value="">
					<p class="help-block">Имя не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<label>Email</label>
					<input class="form-control" placeholder="Email" name="user_email" value="">
					<p class="help-block">Email не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<label>Телефон</label>
					<input class="form-control" placeholder="Телефон" name="user_phone" value="">
					<p class="help-block">Телефон не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<label>Адрес</label>
					<input class="form-control" placeholder="Адрес" name="user_address" value="">
					<p class="help-block">Адрес не должно содержать более 250 символов.</p>
				</div>
				<div class="form-group">
					<label>Пароль</label>
					<input class="form-control" placeholder="Пароль" name="user_pass" value="">
					<p class="help-block">Пароль не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<label>Повторите пароль</label>
					<input class="form-control" placeholder="Повторите пароль" name="confirm_user_pass" value="">
					<p class="help-block">Пароль не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<label>Группа</label>
					<select name="group" class="form-control">
						<option value="">Выберите группу</option>
						<?php foreach($groups as $group){?>
							<option value="<?php echo $group['id_group'];?>"><?php echo $group['group_name'];?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<label>
						<input type="checkbox" name="status" checked> Активный
					</label>
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<script>
	$(function(){
		var add_form = $('#add_form');
		add_form.submit(function (){
			var fdata = add_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/users/ajax_operations/add',
				data: fdata,
				dataType: 'JSON',
				success: function(resp){
					systemMessages(resp.message, resp.mess_type);
				}
			});
			return false;
		});
	});
</script>
