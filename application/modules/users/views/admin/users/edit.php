<div class="col-xs-12 col-sm-6">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<form id="edit_form">
				<div class="form-group">
					<label>Имя</label>
					<input class="form-control" placeholder="Имя" name="user_name" value="<?php echo $user->user_name;?>">
					<p class="help-block">Имя не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<label>Email</label>
					<input class="form-control" placeholder="Email" name="user_email" value="<?php echo $user->user_email;?>">
					<p class="help-block">Email не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<label>Телефон</label>
					<input class="form-control" placeholder="Телефон" name="user_phone" value="<?php echo $user->user_phone;?>">
					<p class="help-block">Телефон не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<label>Адрес</label>
					<input class="form-control" placeholder="Адрес" name="user_address" value="<?php echo $user->user_address;?>">
					<p class="help-block">Адрес не должно содержать более 250 символов.</p>
				</div>
				<div class="form-group">
					<label>Группа</label>
					<select name="group" class="form-control">
						<option value="">Выберите группу</option>
						<?php foreach($groups as $group){?>
							<option value="<?php echo $group['id_group'];?>" <?php echo set_select('group', $group['id_group'], $group['id_group'] == $user->id_group);?>><?php echo $group['group_name'];?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group">
					<label>
						<input type="checkbox" name="status" <?php echo set_checkbox('status', 1, $user->status == 1);?>> Активный
					</label>
				</div>
				<div class="form-group">
					<input type="hidden" name="user" value="<?php echo $user->id;?>">
					<button type="submit" class="btn btn-primary">Сохранить</button>
				</div>
			</form>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<div class="col-xs-12 col-sm-6">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Изменить пароль</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<form role="form" id="change_password_form">
				<div class="form-group">
					<label>Пароль</label>
					<input type="password" class="form-control" placeholder="Пароль" name="user_pass" value="">
					<p class="help-block">Пароль не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<label>Повторите пароль</label>
					<input type="password" class="form-control" placeholder="Повторите пароль" name="confirm_user_pass" value="">
					<p class="help-block">Пароль не должно содержать более 50 символов.</p>
				</div>
				<div class="form-group">
					<input type="hidden" name="user" value="<?php echo $user->id;?>">
					<button type="submit" class="btn btn-success">Изменить пароль</button>
				</div>
			</form>
		</div>
		<!-- /.box-body -->
	</div>
</div>
<script>
	$(function(){
		var edit_form = $('#edit_form');
		edit_form.submit(function (){
			var fdata = edit_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/users/ajax_operations/edit',
				data: fdata,
				dataType: 'JSON',
				success: function(resp){
					systemMessages(resp.message, resp.mess_type);
				}
			});
			return false;
		});

		var change_password_form = $('#change_password_form');
		change_password_form.submit(function (){
			var fdata = change_password_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/users/ajax_operations/change_password',
				data: fdata,
				dataType: 'JSON',
				success: function(resp){
					systemMessages(resp.message, resp.mess_type);
					if(resp.mess_type == 'success'){
						change_password_form[0].reset();
					}
				}
			});
			return false;
		});
	});
</script>
