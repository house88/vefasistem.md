<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/groups/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить группу
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Название</label>
					<input class="form-control" placeholder="Название" name="group_name" value="<?php echo $group['group_name'];?>">
					<p class="help-block">Название не должно содержать более 100 символов.</p>
				</div>
				<div class="form-group">
					<label>Тип</label>
					<select name="group_type" class="form-control">
						<option value="admin" <?php echo set_select('group_type', $group['group_type'], $group['group_type'] == 'admin');?>>Администратор</option>
						<option value="moderator" <?php echo set_select('group_type', $group['group_type'], $group['group_type'] == 'moderator');?>>Модератор</option>
						<option value="content_menedger" <?php echo set_select('group_type', $group['group_type'], $group['group_type'] == 'content_menedger');?>>Контент менеджер</option>
						<option value="user" <?php echo set_select('group_type', $group['group_type'], $group['group_type'] == 'user');?>>Клиент</option>
					</select>
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<input type="hidden" name="group" value="<?php echo $group['id_group'];?>">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<script>
	$(function(){
		var edit_form = $('#edit_form');
		edit_form.submit(function (){
			var fdata = edit_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/users/ajax_operations/edit_group',
				data: fdata,
				dataType: 'JSON',
				success: function(resp){
					systemMessages(resp.message, resp.mess_type);
				}
			});
			return false;
		});
	});
</script>
