<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/rights/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить право
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<div class="box-body">
			<table class="table table-bordered table-striped" id="dtTable">
				<thead>
					<tr>
						<th class="dt_id w-50 text-center">#</th>
						<th class="dt_name">Название</th>
						<th class="dt_alias w-200 text-center">Код</th>
						<th class="dt_actions w-95 text-center">Операций</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<script>
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"sDom": 'ptp',
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/users/rights_list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_id"], "mData": "dt_id"},
				{ "sClass": "vam", "aTargets": ["dt_name"], "mData": "dt_name"},
				{ "sClass": "w-200 text-center vam", "aTargets": ["dt_alias"], "mData": "dt_alias"},
				{ "sClass": "w-95 text-center vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [[0,'asc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type == 'error')
							systemMessages(data.message, 'message-' + data.mess_type);
						if(data.mess_type == 'info')
							systemMessages(data.message, 'message-' + data.mess_type);

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
                }
			}
		});
	});
	var delete_action = function(btn){
		var $this = $(btn);
		var right = $this.data('right');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/users/ajax_operations/delete_right',
			data: {right:right},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>
