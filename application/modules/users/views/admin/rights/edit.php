<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
			<div class="box-tools pull-right">
				<a href="<?php echo base_url('admin/rights/add');?>" class="btn btn-box-tool">
					<i class="fa fa-plus"></i> Добавить право
				</a>
			</div>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="edit_form">			
			<div class="box-body">
				<div class="form-group">
					<label>Название</label>
					<input class="form-control" placeholder="Название" name="right_name" value="<?php echo $right['right_name'];?>">
					<p class="help-block">Название не должно содержать более 250 символов.</p>
				</div>
				<div class="form-group">
					<label>Код</label>
					<p><?php echo $right['right_alias'];?></p>
					<p class="help-block">Название не должно содержать более 50 символов.</p>
				</div>
			</div>
			<!-- /.box-body -->

			<div class="box-footer">
				<input type="hidden" name="right" value="<?php echo $right['id_right'];?>">
				<button type="submit" class="btn btn-primary">Сохранить</button>
			</div>
		</form>
	</div>
</div>
<script>
	$(function(){
		var edit_form = $('#edit_form');
		edit_form.submit(function (){
			var fdata = edit_form.serialize();
			$.ajax({
				type: 'POST',
				url: base_url+'admin/users/ajax_operations/edit_right',
				data: fdata,
				dataType: 'JSON',
				success: function(resp){
					systemMessages(resp.message, resp.mess_type);
				}
			});
			return false;
		});
	});
</script>
