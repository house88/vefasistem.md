<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
        <div class="box-body">
            <div class="directory-search-key-b mt-15"></div>
            <table class="table table-bordered table-striped" id="dtTable">
                <thead>
                    <tr>
                        <th>Описание</th>
                        <?php foreach($groups as $group){?>
                            <th class="mw-100 text-center vam"><?php echo $group['group_name'];?></th>
                        <?php }?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($rights as $right){?>
                        <tr>
                            <td><?php echo $right['right_name'];?></td>
                            <?php foreach($groups as $group){?>
                                <td class="mw-100 vam text-center">
                                    <?php $gr_key = $group['id_group'].'_'.$right['id_right'];?>
                                    <?php if(isset($groups_rights[$gr_key])){?>
                                        <a href="#" class="fs-20 text-success fa fa-check-circle-o call-function" data-callback="change_group_right" data-group="<?php echo $group['id_group'];?>" data-right="<?php echo $right['id_right'];?>"></a>
                                    <?php } else{?>
                                        <a href="#" class="fs-20 text-danger fa fa-times call-function" data-callback="change_group_right" data-group="<?php echo $group['id_group'];?>" data-right="<?php echo $right['id_right'];?>"></a>
                                    <?php }?>
                                </td>
                            <?php }?>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
	</div>
</div>
<script>
	var change_group_right = function(btn){
		var $this = $(btn);
        var group = $this.data('group');
        var right = $this.data('right');
        $.ajax({
            type: 'POST',
            url: base_url+'admin/users/ajax_operations/change_groups_right',
            data: {group:group, right:right},
            dataType: 'JSON',
            success: function(resp){
                systemMessages(resp.message, resp.mess_type);
                if(resp.mess_type == 'success'){
                    $this.toggleClass('test-success text-danger');
                    $this.toggleClass('fa-check-circle-o fa-times');
                }
            }
        });
		return false;
	}
	var delete_action = function(btn){
		var $this = $(btn);
		var news = $this.data('news');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/news/ajax_operations/delete',
			data: {news:news},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					// make changes
				}
			}
		});
		return false;
	}
</script>
