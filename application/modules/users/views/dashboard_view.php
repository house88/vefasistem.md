<div class="row dashboard-page-wr">
	<div class="col-xs-12">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="<?php echo base_url('account');?>">Мои заказы</a></li>
            <li role="presentation"><a href="<?php echo base_url('account/settings');?>">Личные данные</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active">
                <div class="col-xs-12">
                    <?php $this->load->view('orders/user/orders_list');?>
                </div>
            </div>
        </div>
	</div>
</div>