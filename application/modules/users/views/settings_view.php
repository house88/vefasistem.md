<div class="row dashboard-page-wr">
	<div class="col-xs-12">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation"><a href="<?php echo base_url('account');?>">Мои заказы</a></li>
            <li role="presentation" class="active"><a href="<?php echo base_url('account/settings');?>">Личные данные</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="profile">
                <div class="tab-wr">
                    <div class="col-sm-6 col-xs-12">
                        <form>
                            <div class="form-group">
                                <label>Имя</label>
                                <input type="text" class="form-control" placeholder="Имя" name="name" value="<?php echo $user->user_name;?>">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo $user->user_email;?>">
                            </div>
                            <div class="form-group">
                                <label>Телефон</label>
                                <input type="text" class="form-control" placeholder="Телефон" name="phone" value="<?php echo $user->user_phone;?>">
                            </div>
                            <div class="form-group">
                                <label>Адрес</label>
                                <input type="text" class="form-control" placeholder="Адрес" name="address" value="<?php echo $user->user_address;?>">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success pull-right call-function" data-callback="update_personal_settings" type="button">Сохранить изменения</button>
                            </div>
                        </form> 
                    </div> 
                    <div class="col-sm-6 col-xs-12">
                        <form>
                            <div class="form-group">
                                <label>Нынешний пароль</label>
                                <input type="password" name="old_password" class="form-control" placeholder="Нынешний пароль">
                            </div>
                            <div class="form-group">
                                <label>Новый пароль</label>
                                <input type="password" name="password" class="form-control" placeholder="Новый пароль">
                            </div>
                            <div class="form-group">
                                <label>Повторите новый пароль</label>
                                <input type="password" name="confirm_password" class="form-control" placeholder="Повторите новый пароль">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success pull-right call-function" data-callback="update_personal_password" type="button">Изменить пароль</button>
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>