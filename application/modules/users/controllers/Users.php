<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Users extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		
        $this->data = array();
        $this->breadcrumbs = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}
	
	function index(){
		return_404($this->data);
	}
	
	function account(){
		if(!$this->auth_lib->logged_in()){
			redirect('signin');
		}

		$this->breadcrumbs[] = array(
			'title' => 'Личный кабинет',
			'link' => base_url('account')
		);

		$this->data['stitle'] = 'Личный кабинет';

		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'dashboard_view';
		$this->load->view('shop/page_template', $this->data);
		
	}

	function account_settings(){
		if(!$this->auth_lib->logged_in()){
			redirect('signin');
		}
		
		$this->breadcrumbs[] = array(
			'title' => 'Личный кабинет',
			'link' => base_url('account')
		);

		$this->breadcrumbs[] = array(
			'title' => 'Личные данные',
			'link' => base_url('account/settings')
		);

		$this->data['stitle'] = 'Личные данные';

		$this->data['user'] = $this->auth_lib->user_data();
		$this->data['breadcrumbs'] = $this->breadcrumbs;
		$this->data['main_content'] = 'settings_view';
		$this->load->view('shop/page_template', $this->data);
	}

	function ajax_operations(){
		if (!$this->input->is_ajax_request()) {
		   exit('No direct script access allowed');
		}

		if(!$this->auth_lib->logged_in()){
			jsonResponse('Вы не авторизированы.');
		}

		$action = $this->uri->segment(3);
		switch ($action) {
			case 'update':
				$user = $this->auth_lib->user_data();
				$config = array(
					array(
						'field' => 'name',
						'label' => 'Имя',
						'rules' => 'required|trim|xss_clean|max_length[50]',
					),
					array(
						'field' => 'phone',
						'label' => 'Телефон',
						'rules' => 'xss_clean|max_length[50]',
					),
					array(
						'field' => 'address',
						'label' => 'Адрес',
						'rules' => 'xss_clean|max_length[250]',
					)
				);

				if($user->user_email != $this->input->post('email')){
					$config[] = array(
						'field' => 'email',
						'label' => 'E-mail',
						'rules' => 'trim|required|valid_email|is_unique[users.user_email]|xss_clean',
					);
				} else{
					$config[] = array(
						'field' => 'email',
						'label' => 'E-mail',
						'rules' => 'trim|required|valid_email|xss_clean',
					);
				}

				$this->form_validation->set_rules($config);

				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$update = array(
					'user_name' => $this->input->post('name',true),
					'user_email' => $this->input->post('email',true),
					'user_phone' => $this->input->post('phone',true),
					'user_address' => $this->input->post('address',true)
				);
				
				$this->auth_model->handler_update($user->id, $update);

				jsonResponse('Сохраненно.', 'success');
			break;
			case 'change_password':
				$user = $this->auth_lib->user_data();
				$config = array(
					array(
						'field' => 'old_password',
						'label' => 'Нынешний пароль',
						'rules' => 'required|trim|xss_clean',
					),
					array(
						'field' => 'password',
						'label' => 'Новый пароль',
						'rules' => 'required|trim|xss_clean|min_length[6]',
					),
					array(
						'field' => 'confirm_password',
						'label' => 'Повторите новый пароль',
						'rules' => 'required|trim|xss_clean|matches[password]',
					)
				);

				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

				$old_password = $this->auth_lib->hash_password($this->input->post('old_password', true));
				if($old_password != $user->user_pass){
					jsonResponse('Нынешний пароль верный.');
				}

				$update = array(
					'reset_key' => md5(rand(0,1000).'@ccreset'),
					'user_pass' => $this->auth_lib->hash_password($this->input->post('password',true))
				);
				$this->auth_model->handler_update($user->id, $update);

				jsonResponse('Пароль был изменен.', 'success');
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		}
	}
}

?>