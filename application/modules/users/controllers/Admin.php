<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->load->model("User_model", 'user_model');

		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Error: Please signin first.');
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Пользователи';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->load->model("menu/Menu_model", "menu");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		if(!$this->auth_lib->have_right('manage_users')){
			redirect('/admin');
		}

		$this->data['main_content'] = 'admin/users/list';
		$this->load->view('admin/page', $this->data);
	}

    function add(){
		if(!$this->auth_lib->have_right('manage_users')){
			redirect('/admin');
		}

		$this->data['main_title'] = 'Добавить пользователя';
        $this->data['groups'] = $this->user_model->handler_get_groups_all();
		$this->data['main_content'] = 'admin/users/add';
		$this->load->view('admin/page', $this->data);
    }

    function edit(){
		if(!$this->auth_lib->have_right('manage_users')){
			redirect('/admin');
		}

		$id_user = (int)$this->uri->segment(4);
		$this->data['user'] = $this->user_model->handler_get($id_user);
        if(empty($this->data['user'])){
            redirect('admin/users');
        }

        $this->data['groups'] = arrayByKey($this->user_model->handler_get_groups_all(), 'id_group');
		$this->data['main_title'] = 'Редактировать пользователя';
		$this->data['main_content'] = 'admin/users/edit';
		$this->load->view('admin/page', $this->data);
    }

	function groups(){
		if(!$this->auth_lib->have_right('manage_groups')){
			redirect('/admin');
		}

        $action = $this->uri->segment(3);
        switch ($action) {
            case 'add':
		        $this->data['main_title'] = 'Добавление групы';
                $this->data['main_content'] = 'admin/groups/add';
		        $this->load->view('admin/page', $this->data);
            break;            
            case 'edit':
		        $this->data['main_title'] = 'Редактирование групы';
                $id_group = (int)$this->uri->segment(4);
                $this->data['group'] = $this->user_model->handler_get_group($id_group);
                $this->data['main_content'] = 'admin/groups/edit';
                $this->load->view('admin/page', $this->data);
            break;
            default:
		        $this->data['main_title'] = 'Групы';
                $this->data['main_content'] = 'admin/groups/list';
		        $this->load->view('admin/page', $this->data);
            break;
        }
    }
        
	function rights(){
		if(!$this->auth_lib->have_right('manage_rights')){
			redirect('/admin');
		}

        $action = $this->uri->segment(3);
        switch ($action) {
            case 'add':
		        $this->data['main_title'] = 'Добавление право';
                $this->data['main_content'] = 'admin/rights/add';
		        $this->load->view('admin/page', $this->data);
            break;            
            case 'edit':
		        $this->data['main_title'] = 'Редактирование право';
                $id_right = (int)$this->uri->segment(4);
                $this->data['right'] = $this->user_model->handler_get_right($id_right);
                $this->data['main_content'] = 'admin/rights/edit';
                $this->load->view('admin/page', $this->data);
            break;            
            default:
		        $this->data['main_title'] = 'Права';
                $this->data['main_content'] = 'admin/rights/list';
                $this->load->view('admin/page', $this->data);
            break;
        }
	}

	function group_right(){
		if(!$this->auth_lib->have_right('manage_group_rights')){
			redirect('/admin');
		}

        $this->data['main_title'] = 'Права по группам';
        $this->data['groups'] = $this->user_model->handler_get_groups_all();
        $this->data['rights'] = $this->user_model->handler_get_rights_all();
        $this->data['groups_rights'] = arrayByKey($this->user_model->handler_get_groups_rights_all(), 'gr_key');
		$this->data['main_content'] = 'admin/group_rights';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
            case 'add':
                if(!$this->auth_lib->have_right('manage_users')){
                    jsonResponse('Ошибка: Нет прав!');
                }

				$this->form_validation->set_rules('user_name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_email', 'Email', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_phone', 'Телефон', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_address', 'Адрес', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('user_pass', 'Пароль', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('confirm_user_pass', 'Повторите пароль', 'required|xss_clean|max_length[50]|matches[user_pass]');
				$this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
                    'id_group' => (int) $this->input->post('group'),
					'user_name' => $this->input->post('user_name'),
					'user_email' => $this->input->post('user_email'),
					'user_pass' => $this->auth_lib->hash_password($this->input->post('user_pass',true)),
					'user_phone' => $this->input->post('user_phone'),
					'user_address' => $this->input->post('user_address'),
					'registered_date' => date('Y-m-d H:i:s'),
					'activation_key' => md5(rand(0,1000).'@cc@ctivate'),
                    'status' => ($this->input->post('status', true))?1:0
				);

                $group = $this->user_model->handler_get_group($insert['id_group']);
                if(empty($group)){
                    jsonResponse('Ошибка: нет такой группы.');
                }

                if($group['group_type'] == 'content_menedger'){                    
                    $categories = $this->input->post('category_parent');
                    if(!empty($categories)){
		                $this->load->model("categories/Categories_model", "categories");
                        $categories_info = $this->categories->handler_get_all(array('id_category' => $categories));
                        $clist = array();
                        $clist_full = array();
                        foreach ($categories_info as $category) {
                            $clist[] = $category['category_id'];
                            $cbreads = json_decode('['.$category['category_breadcrumbs'].']');
                            foreach ($cbreads as $cbread) {
                                $clist_full[$cbread->category_id] = $cbread->category_id;
                            }
                        }
                        $insert['user_categories_access'] = implode(',', $clist);
                        $insert['user_categories_access_full'] = implode(',', $clist_full);
                    }
                }

				$this->auth_model->handler_insert($insert);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'edit':
                if(!$this->auth_lib->have_right('manage_users')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('user', 'Пользователь', 'required|xss_clean');
				$this->form_validation->set_rules('user_name', 'Имя', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_email', 'Email', 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('user_address', 'Адрес', 'xss_clean|max_length[250]');
				$this->form_validation->set_rules('user_phone', 'Телефон', 'xss_clean|max_length[50]');
				$this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_user = (int)$this->input->post('user');
				$update = array(
					'user_name' => $this->input->post('user_name'),
					'user_email' => $this->input->post('user_email'),
					'user_phone' => $this->input->post('user_phone'),
					'user_address' => $this->input->post('user_address'),
                    'id_group' => (int) $this->input->post('group'),
                    'user_categories_access' => ''
				);

                $group = $this->user_model->handler_get_group($update['id_group']);
                if(empty($group)){
                    jsonResponse('Ошибка: нет такой группы.');
                }

                if($group['group_type'] == 'content_menedger'){                    
                    $categories = $this->input->post('category_parent');
                    if(!empty($categories)){
                        $this->load->model("categories/Categories_model", "categories");
                        $categories_info = $this->categories->handler_get_all(array('id_category' => $categories));
                        $clist = array();
                        $clist_full = array();
                        foreach ($categories_info as $category) {
                            $clist[] = $category['category_id'];
                            $cbreads = json_decode('['.$category['category_breadcrumbs'].']');
                            foreach ($cbreads as $cbread) {
                                $clist_full[$cbread->category_id] = $cbread->category_id;
                            }
                        }
                        $update['user_categories_access'] = implode(',', $clist);
                        $update['user_categories_access_full'] = implode(',', $clist_full);
                    }
                }

				$this->auth_model->handler_update($id_user, $update);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'change_password':
                if(!$this->auth_lib->have_right('manage_users')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $config = array(
					array(
						'field' => 'user',
						'label' => 'Пользователь',
						'rules' => 'required|xss_clean',
					),
					array(
						'field' => 'user_pass',
						'label' => 'Пароль',
						'rules' => 'required|trim|xss_clean|min_length[6]',
					),
					array(
						'field' => 'confirm_user_pass',
						'label' => 'Повторите пароль',
						'rules' => 'required|trim|xss_clean|matches[user_pass]',
					)
				);

				$this->form_validation->set_rules($config);
				if($this->form_validation->run() === false){
					jsonResponse($this->form_validation->error_array());
				}

                $id_user = (int) $this->input->post('user');
				$update = array(
					'reset_key' => md5(rand(0,1000).'@ccreset'),
					'user_pass' => $this->auth_lib->hash_password($this->input->post('user_pass',true))
				);
				$this->auth_model->handler_update($id_user, $update);

				jsonResponse('Пароль был изменен.', 'success');
            break;
			case 'change_banned_status':
                if(!$this->auth_lib->have_right('manage_users')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('user', 'Пользователь', 'required|xss_clean');
				if ($this->form_validation->run() == false){
					jsonResponse($this->form_validation->error_array());
				}

				$id_user = (int)$this->input->post('user');
				$user = $this->auth_model->handler_get($id_user);
				if(empty($user)){
					jsonResponse('Не могу завершить операцию.');
				}

				if($user->user_banned == 1){
					$status = 0;
				} else{
					$status = 1;
				}

				$this->auth_model->handler_update($id_user, array('user_banned' => $status));
				jsonResponse('Операция прошла успешно.', 'success', array('status' => $status));
			break;
            case 'add_group':
                if(!$this->auth_lib->have_right('manage_groups')){
                    jsonResponse('Ошибка: Нет прав!');
                }

				$this->form_validation->set_rules('group_name', 'Название', 'required|xss_clean|max_length[100]');
				$this->form_validation->set_rules('group_type', 'Тип', 'required');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'group_name' => $this->input->post('group_name'),
					'group_type' => $this->input->post('group_type')
				);

				$this->user_model->handler_insert_group($insert);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'edit_group':
                if(!$this->auth_lib->have_right('manage_groups')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');
				$this->form_validation->set_rules('group_name', 'Название', 'required|xss_clean|max_length[100]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_group = (int)$this->input->post('group');
				$update = array(
					'group_name' => $this->input->post('group_name'),
					'group_type' => $this->input->post('group_type')
				);

				$this->user_model->handler_update_group($id_group, $update);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'delete_group':
                if(!$this->auth_lib->have_right('manage_groups')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');                

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_group = (int)$this->input->post('group');
                if($this->user_model->handler_get_group_rights($id_group)){
				    jsonResponse('Удалите все связи с правами для данной группы.');
                }
                
                if($this->user_model->handler_get_group_users($id_group)){
				    jsonResponse('Удалите все связи с пользователями для данной группы.');
                }

				$this->user_model->handler_delete_group($id_group);
				jsonResponse('Удалено.', 'success');
            break;
            case 'add_right':
                if(!$this->auth_lib->have_right('manage_rights')){
                    jsonResponse('Ошибка: Нет прав!');
                }

				$this->form_validation->set_rules('right_name', 'Название', 'required|xss_clean|max_length[250]');
				$this->form_validation->set_rules('right_alias', 'Код', 'required|xss_clean|max_length[50]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$insert = array(
					'right_name' => $this->input->post('right_name'),
					'right_alias' => $this->input->post('right_alias')
				);

				$this->user_model->handler_insert_right($insert);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'edit_right':
                if(!$this->auth_lib->have_right('manage_rights')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('right', 'Право', 'required|xss_clean');
				$this->form_validation->set_rules('right_name', 'Название', 'required|xss_clean|max_length[250]');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_right = (int)$this->input->post('right');
				$update = array(
					'right_name' => $this->input->post('right_name')
				);

				$this->user_model->handler_update_right($id_right, $update);
				jsonResponse('Сохранено.', 'success');
            break;
            case 'delete_right':
                if(!$this->auth_lib->have_right('manage_rights')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('right', 'Право', 'required|xss_clean');                

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_right = (int)$this->input->post('right');
                if($this->user_model->handler_get_right_groups($id_right)){
				    jsonResponse('Удалите все связи с группами для данного права.');
                }

				$this->user_model->handler_delete_right($id_right);
				jsonResponse('Удалено.', 'success');
            break;
            case 'change_groups_right':
                if(!$this->auth_lib->have_right('manage_group_rights')){
                    jsonResponse('Ошибка: Нет прав!');
                }

				$this->form_validation->set_rules('group', 'Группа', 'required|xss_clean');
				$this->form_validation->set_rules('right', 'Право', 'required|xss_clean');

                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $group = (int) $this->input->post('group');
                $right = (int) $this->input->post('right');
                if($this->user_model->handler_get_group_right($group, $right)){
                    $this->user_model->handler_delete_group_right($group, $right);
                } else{
                    $insert = array(
                        'id_group' => $group,
                        'id_right' => $right
                    );
                    $this->user_model->handler_insert_group_right($insert);
                }
				jsonResponse('Сохранено.', 'success');
            break;
		}
	}

	function users_list_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if(!$this->auth_lib->have_right('manage_users')){
            jsonDTResponse('Ошибка: Нет прав!');
        }

		$params = array(
            'limit' => intVal($_POST['iDisplayLength']),
            'start' => intVal($_POST['iDisplayStart']),
            'group_type' => 'user'
        );

        if ($_POST['iSortingCols'] > 0) {
            for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
                switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
                    case 'dt_id': $params['sort_by'][] = 'id-' . $_POST['sSortDir_' . $i];
                    break;
                    case 'dt_name': $params['sort_by'][] = 'user_name-' . $_POST['sSortDir_' . $i];
                    break;
                    case 'dt_email': $params['sort_by'][] = 'user_email-' . $_POST['sSortDir_' . $i];
                    break;
                    case 'dt_regdate': $params['sort_by'][] = 'registered_date-' . $_POST['sSortDir_' . $i];
                    break;
                }
            }
        }

		if(isset($_POST['registered_date_from'])){
			$params['registered_date_from'] = formatDate($this->input->post('registered_date_from'), 'Y-m-d');
		}

		if(isset($_POST['registered_date_to'])){
			$params['registered_date_to'] = formatDate($this->input->post('registered_date_to'), 'Y-m-d');
		}

		if(isset($_POST['keywords'])){
			$params['keywords'] = $this->input->post('keywords');
		}

        $records = $this->user_model->handler_get_all($params);
        $records_total = $this->user_model->handler_get_count($params);

        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $records_total,
            "iTotalDisplayRecords" => $records_total,
            "aaData" => array()
        );

        foreach ($records as $record) {
			$status = '<a href="#" data-message="Вы уверены что хотите заблокировать пользователя?" title="Заблокировать" data-title="Заблокировать пользователя" data-callback="change_banned_status" data-user="'.$record['id'].'" class="btn btn-success btn-xs confirm-dialog"><i class="fa fa-eye"></i></a>';
			if($record['user_banned'] == 1){
				$status = '<a href="#" data-message="Вы уверены что хотите разаблокировать пользователя?" title="Разаблокировать" data-title="Разаблокировать пользователя" data-callback="change_banned_status" data-user="'.$record['id'].'" class="btn btn-danger btn-xs confirm-dialog"><i class="fa fa-eye-slash"></i></a>';
			}
            $output['aaData'][] = array(
                'dt_id'			=>  $record['id'],
                'dt_name'		=>  $record['user_name'],
                'dt_email'		=>  $record['user_email'],
                'dt_phone'		=>  $record['user_phone'],
                'dt_address'	=>  $record['user_address'],
                'dt_regdate'	=>  formatDate($record['registered_date'], 'd/m/Y H:i:s'),
				'dt_actions'	=>  '<div class="btn-toolbar">
										<div class="btn-group mb-5">
											<a href="'.base_url('admin/users/edit/'.$record['id']).'" title="Редактировать" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
											'.$status.'
										</div>
									</div>'
            );
        }
        jsonResponse('', 'success', $output);
	}

	function groups_list_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if(!$this->auth_lib->have_right('manage_groups')){
            jsonDTResponse('Ошибка: Нет прав!');
        }

		$params = array(
            'limit' => intVal($_POST['iDisplayLength']),
            'start' => intVal($_POST['iDisplayStart'])
        );

        if ($_POST['iSortingCols'] > 0) {
            for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
                switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
                    case 'dt_id': $params['sort_by'][] = 'id_group-' . $_POST['sSortDir_' . $i];
                    break;
                    case 'dt_name': $params['sort_by'][] = 'group_name-' . $_POST['sSortDir_' . $i];
                    break;
                    case 'dt_type': $params['sort_by'][] = 'group_type-' . $_POST['sSortDir_' . $i];
                    break;
                }
            }
        }

        $records = $this->user_model->handler_get_groups_all($params);
        $records_total = $this->user_model->handler_get_groups_count($params);

        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $records_total,
            "iTotalDisplayRecords" => $records_total,
            "aaData" => array()
        );

        $group_types = array(
            'admin' => array(
                'title' => 'Администратор'
            ),
            'moderator' => array(
                'title' => 'Модератор'
            ),
            'content_menedger' => array(
                'title' => 'Контент менеджер'
            ),
            'user' => array(
                'title' => 'Пользователь'
            )
        );

        foreach ($records as $record) {
            $output['aaData'][] = array(
                'dt_id'			=>  $record['id_group'],
                'dt_name'		=>  $record['group_name'],
                'dt_type'		=>  $group_types[$record['group_type']]['title'],
				'dt_actions'	=>  '<a href="'.base_url().'admin/groups/edit/'.$record['id_group'].'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                    <a href="#" class="btn btn-danger btn-xs confirm-dialog" data-message="Вы уверены что хотите удалить группу?" data-callback="delete_action" data-group="'.$record['id_group'].'"><i class="fa fa-times"></i></a>'
            );
        }
        jsonResponse('', 'success', $output);
	}
    
	function rights_list_dt(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        if(!$this->auth_lib->have_right('manage_rights')){
            jsonDTResponse('Ошибка: Нет прав!');
        }

		$params = array(
            'limit' => intVal($_POST['iDisplayLength']),
            'start' => intVal($_POST['iDisplayStart'])
        );

        if ($_POST['iSortingCols'] > 0) {
            for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
                switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
                    case 'dt_id': $params['sort_by'][] = 'id_right-' . $_POST['sSortDir_' . $i];
                    break;
                    case 'dt_name': $params['sort_by'][] = 'right_name-' . $_POST['sSortDir_' . $i];
                    break;
                }
            }
        }

        $records = $this->user_model->handler_get_rights_all($params);
        $records_total = $this->user_model->handler_get_rights_count($params);

        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $records_total,
            "iTotalDisplayRecords" => $records_total,
            "aaData" => array()
        );

        foreach ($records as $record) {
            $delete_btn = '';
            if($record['right_cant_delete'] == 0){
                $delete_btn = '<a href="#" class="btn btn-danger btn-xs confirm-dialog" data-message="Вы уверены что хотите удалить право?" data-callback="delete_action" data-right="'.$record['id_right'].'"><i class="fa fa-times"></i></a>';
            }
            $output['aaData'][] = array(
                'dt_id'			=>  $record['id_right'],
                'dt_name'		=>  $record['right_name'],
                'dt_alias'		=>  $record['right_alias'],
				'dt_actions'	=>  '<a href="'.base_url().'admin/rights/edit/'.$record['id_right'].'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a> '
                                    .$delete_btn
            );
        }
        jsonResponse('', 'success', $output);
	}
}
?>