<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MX_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model("menu_model");
        $this->data = array();
	}

	function _show_menu($menu_params = array()){
		extract($menu_params);
		if(!isset($menu_title)){
			return;
		}

		if(!isset($menu_block)){
			return;
		}

		$menu = $this->menu_model->handler_get_by_title($menu_title);
		$this->data['menu_elements'] = json_decode($menu['menu_elements'], true);
		switch($menu_block){
			case 'footer' :
				$this->load->view("footer_menu", $this->data);
			break;
		}
	}	
}

?>
