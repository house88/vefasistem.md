<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();
        
		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Error: Please signin first.');
			}
		}

		$this->data = array();

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

        $this->load->model("menu/Menu_model", "menu");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

    function index(){
		if(!$this->auth_lib->have_right('manage_site_menu')){
			redirect('/admin');
		}

        $this->data['left_menu_items'] = $this->menu->handler_get_all();
        $this->data['main_content'] = 'admin/menu_list';
        $this->load->view('admin/page', $this->data);
    }

    function add(){
		if(!$this->auth_lib->have_right('manage_site_menu')){
			redirect('/admin');
		}

        $this->data['main_content'] = 'admin/menu_add';
        $this->load->view('admin/page', $this->data);
    }

    function edit(){
		if(!$this->auth_lib->have_right('manage_site_menu')){
			redirect('/admin');
		}

        $id_menu = (int)$this->uri->segment(4);
        $this->data['menu_item'] = $this->menu->handler_get($id_menu);
        if(empty($this->data['menu_item'])){
            redirect('admin/menu');
        }
        $this->data['main_content'] = 'admin/menu_edit';
        $this->load->view('admin/page', $this->data);
    }

    function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $option = $this->uri->segment(4);
        switch($option){
            case 'add':
                if(!$this->auth_lib->have_right('manage_site_menu')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$menu_elements = $this->input->post('menu_elements');
				$valid_menu_elements = array();
				if(!empty($menu_elements)){
					foreach($menu_elements as $menu_element){
						if(empty($menu_element['title'])){
							jsonResponse('Названия елементов меню не могум быть пустыми.');
							break;
						}
						if(empty($menu_element['link'])){
							jsonResponse('Ссылки елементов меню не могум быть пустыми.');
							break;
						}
						$valid_menu_elements[] = $menu_element;
					}
				}
                $insert = array(
                    'menu_title' => $this->input->post('title'),
					'menu_elements' => json_encode($valid_menu_elements)
                );
                $this->menu->handler_insert($insert);
                jsonResponse('Сохранено.', 'success');
            break;
            case 'edit':
                if(!$this->auth_lib->have_right('manage_site_menu')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('title', 'Название', 'required|xss_clean|max_length[100]');
                $this->form_validation->set_rules('menu', 'Раздел', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $id_menu = (int)$this->input->post('menu');
				$menu_elements = $this->input->post('menu_elements');
				$valid_menu_elements = array();
				if(!empty($menu_elements)){
					foreach($menu_elements as $menu_element){
						if(empty($menu_element['title'])){
							jsonResponse('Названия елементов меню не могум быть пустыми.');
							break;
						}
						if(empty($menu_element['link'])){
							jsonResponse('Ссылки елементов меню не могум быть пустыми.');
							break;
						}
						$valid_menu_elements[] = $menu_element;
					}
				}
                $update = array(
                    'menu_title' => $this->input->post('title'),
					'menu_elements' => json_encode($valid_menu_elements)
                );
                $this->menu->handler_update($id_menu, $update);
                jsonResponse('Сохранено.', 'success');
            break;
            case 'delete':
                if(!$this->auth_lib->have_right('manage_site_menu')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('menu', 'Раздел', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $id_menu = (int)$this->input->post('menu');
                $this->menu->handler_delete($id_menu);
                jsonResponse('Операция прошла успешно.', 'success');
            break;
            case 'change_status':
                if(!$this->auth_lib->have_right('manage_site_menu')){
                    jsonResponse('Ошибка: Нет прав!');
                }

                $this->form_validation->set_rules('menu', 'Раздел', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

                $id_menu = (int)$this->input->post('menu');
        		$menu_item = $this->menu->handler_get($id_menu);
				if(empty($menu_item)){
					jsonResponse('Раздел не найден.');
				}

				if($menu_item['menu_visible'] == 1){
					$status = 0;
				} else{
					$status = 1;
				}
                $update = array(
                    'menu_visible' => $status
                );
                $this->menu->handler_update($id_menu, $update);
                jsonResponse('Сохранено.', 'success', array('status' => $status));
            break;
        }
    }
}

?>
