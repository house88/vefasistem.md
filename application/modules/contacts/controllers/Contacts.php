<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contacts extends MX_Controller{
	function __construct(){
		parent::__construct();

		$this->data = array();
		$this->breadcrumbs = array();
		$this->load->model("admin/Settings_model", "settings");
		$this->load->model("Contacts_model", "contacts");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');
		$this->data['stitle'] = $this->data['settings']['default_title']['setting_value'];
		$this->data['skeywords'] = $this->data['settings']['mk']['setting_value'];
		$this->data['sdescription'] = $this->data['settings']['md']['setting_value'];
        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		show_404();
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'callme':
				$this->form_validation->set_rules('name', lang_line('form_contact_name', false), 'required|xss_clean|max_length[50]');
				$this->form_validation->set_rules('phone', lang_line('form_contact_phone', false), 'xss_clean|max_length[12]');
				$this->form_validation->set_rules('email', lang_line('form_contact_email', false), 'valid_email|xss_clean|max_length[50]');
				$this->form_validation->set_rules('messages', lang_line('form_contact_comment', false), 'xss_clean|max_length[2000]');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
				}
				
				if(!$this->input->post('phone') && !$this->input->post('email')){
					jsonResponse(lang_line('error_message_callme_phone_email_require', false));
				}

				$insert = array(
					'contact_name' => $this->input->post('name', true),
					'contact_email' => $this->input->post('phone', true),
					'contact_phone' => $this->input->post('email', true),
					'contact_text_client' => $this->input->post('messages', true),
					'contact_date' => $this->contacts->handler_get_db_date()
				);
				$this->contacts->handler_insert($insert);

				$email_data = array(
					'contact_info' => $insert,
					'title' => "Сообщение от {$insert['contact_name']} на сайте vefasistem.md",
					'email_content' => 'callme_admin_tpl'
				);
				$this->load->library('email');
				
				$this->email->from('noreply@vefasistem.md', $this->data['settings']['default_title']['setting_value']);
				$this->email->to($this->data['settings']['callme_email']['setting_value']);					
				$this->email->subject("Сообщение от {$insert['contact_name']} на сайте vefasistem.md");
				$this->email->message($this->load->view('email_templates/email_tpl', $email_data, true));	
				$this->email->set_mailtype("html");
				$this->email->send();

				jsonResponse(lang_line('success_message_callme', false), 'success');
			break;
		}
	}

	function popup(){
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		 }
 
		 $action = $this->uri->segment(4);
		 switch ($action) {
			case 'callme':
				$content = $this->load->view('contacts/popup_form_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			case 'dealer':
				$content = $this->load->view('contacts/popup_form_dealer_view', $this->data, true);
				jsonResponse('', 'success', array('popup_content' => $content));
			break;
			default:
				jsonResponse('Данные не верны.');
			break;
		 }
	}
}
?>