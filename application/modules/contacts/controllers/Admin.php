<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller{
	function __construct(){
		parent::__construct();

		if(!$this->auth_lib->is_admin()){
			if (!$this->input->is_ajax_request()) {
				redirect('/');
			} else{
				jsonResponse('Error: Please signin first.');
			}
		}

		$this->data = array();
		$this->data['main_title'] = 'Обратная связь';

		$this->load->model("admin/Settings_model", "settings");
		$this->data['settings'] = arrayByKey($this->settings->get_settings(), 'setting_alias');

		$this->load->model("Contacts_model", "contacts");

        $this->data['system_messages'] = (! isset($this->data['system_messages'])) ? $this->session->flashdata('system_messages') : $this->data['system_messages'];
	}

	function index(){
		$this->data['main_content'] = 'admin/list';
		$this->load->view('admin/page', $this->data);
	}

	function ajax_operations(){
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

		$option = $this->uri->segment(4);
		switch($option){
			case 'delete':
				$this->form_validation->set_rules('contact', 'Контакт', 'required|xss_clean');
                if ($this->form_validation->run() == false){
                    jsonResponse($this->form_validation->error_array());
                }

				$id_contact = (int)$this->input->post('contact');
				$contact = $this->contacts->handler_get($id_contact);
				if(empty($contact)){
					jsonResponse('Данные не верны.');
				}

				$this->contacts->handler_delete($id_contact);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'change':
				$contact = $this->input->post('contact');
				if(empty($contact)){
					jsonResponse('Данные не верны!');
				}

				$id_contact = (int) $contact['id_contact'];
				$contact_info = $this->contacts->handler_get($id_contact);
				if(empty($contact_info)){
					jsonResponse('Данные не верны!');
				}

				$status = (int)$contact['status'];
				$statuses = get_contact_statuses();				
				if(!array_key_exists($status, $statuses)){
					jsonResponse('Данные не верны.');
				}

				$update = array(
					'contact_status' => $status,
					'contact_text_admin' => xss_clean($contact['admin_comment'])
				);

				$this->contacts->handler_update($id_contact, $update);
				jsonResponse('Операция прошла успешно.', 'success');
			break;
			case 'list_dt':
				$params = array(
					'limit' => intVal($_POST['iDisplayLength']),
					'start' => intVal($_POST['iDisplayStart'])
				);
		
				if ($_POST['iSortingCols'] > 0) {
					for ($i = 0; $i < $_POST['iSortingCols']; $i++) {
						switch ($_POST["mDataProp_" . intval($_POST['iSortCol_' . $i])]) {
							case 'dt_id': $params['sort_by'][] = 'id_contact-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_update': $params['sort_by'][] = 'contact_date_update-' . $_POST['sSortDir_' . $i];
							break;
							case 'dt_create': $params['sort_by'][] = 'contact_date-' . $_POST['sSortDir_' . $i];
							break;
						}
					}
				}
		
				$records = $this->contacts->handler_get_all($params);
				$records_total = $this->contacts->handler_get_count($params);
		
				$output = array(
					"sEcho" => intval($_POST['sEcho']),
					"iTotalRecords" => $records_total,
					"iTotalDisplayRecords" => $records_total,
					"aaData" => array()
				);
				// get_contact_status
				// get_contact_statuses
				foreach ($records as $record) {
					$status = get_contact_status($record['contact_status']);
					$contact_detail = $this->load->view('admin/contact_view', array('contact' => $record), true);
					$output['aaData'][] = array(
						'dt_toggle'		=> '<a class="call-function" data-callback="toggle_detail" href="#"><span class="fa fa-plus"></span></a>',
						'dt_name'		=>  $record['contact_name'],
						'dt_email'		=>  $record['contact_email'],
						'dt_phone'		=>  $record['contact_phone'],
						'dt_create'		=>  formatDate($record['contact_date'], 'd/m/Y H:i:s'),
						'dt_update'		=>  formatDate($record['contact_date_update'], 'd/m/Y H:i:s'),
						'dt_status' 	=> '<div class="label label-'.$status['color_class'].'">'.$status['title'].'</div>',
						'dt_detail'		=>  $contact_detail,
						'dt_actions'	=>  '<div class="btn-group">
												<a href="#" class="btn btn-danger btn-xs confirm-dialog" title="Удалить" data-message="Вы уверены что хотите удалить запись?" data-callback="delete_action" data-contact="'.$record['id_contact'].'"><i class="fa fa-remove"></i></a>
											</div>
											'
						
					);
				}
				jsonResponse('', 'success', $output);
			break;
		}
	}
}
?>