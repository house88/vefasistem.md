<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contacts_model extends CI_Model{
	var $contacts = "contacts";
	function __construct(){
		parent::__construct();
	}

	function handler_get_db_date(){
		$this->db->select("NOW() as db_time");
		$query = $this->db->get()->row_array();
		return $query['db_time'];
	}

	function handler_insert($data = array()){
		if(empty($data)){
			return;
		}

		$this->db->insert($this->contacts, $data);
		return $this->db->insert_id();
	}

	function handler_update($id_contact = 0, $data = array()){
		if(empty($data)){
			return;
		}

		$this->db->where('id_contact', $id_contact);
		$this->db->update($this->contacts, $data);
	}

	function handler_delete($id_contact = 0){
		$this->db->where('id_contact', $id_contact);
		$this->db->delete($this->contacts);
	}

	function handler_get($id_contact = 0){
		$this->db->where('id_contact', $id_contact);
		return $this->db->get($this->contacts)->row_array();
	}

	function handler_get_all($conditions = array()){
        $order_by = " id_contact ASC ";

        extract($conditions);

		if (isset($sort_by)) {
			foreach ($sort_by as $sort_item) {
				$sort_item = explode('-', $sort_item);
				$multi_order_by[] = $sort_item[0] . ' ' . $sort_item[1];
			}
			$order_by = implode(',', $multi_order_by);
		}

        if(isset($id_contact)){
			$this->db->where_in('id_contact', $id_contact);
        }

        if(isset($contact_status)){
			$this->db->where_in('contact_status', $contact_status);
        }

		$this->db->order_by($order_by);

		if(isset($limit, $start)){
			$this->db->limit($limit, $start);
		}

		return $this->db->get($this->contacts)->result_array();
	}

	function handler_get_count($conditions = array()){
        extract($conditions);

        if(isset($id_contact)){
			$this->db->where_in('id_contact', $id_contact);
        }

        if(isset($contact_status)){
			$this->db->where_in('contact_status', $contact_status);
        }

		return $this->db->count_all_results($this->contacts);
	}
}
?>
