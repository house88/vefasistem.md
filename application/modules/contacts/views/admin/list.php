<div class="col-xs-12">
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title"><?php echo $main_title;?></h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form id="settings_form">			
			<div class="box-body">
				<div class="directory-search-key-b mt-15"></div>
				<table class="table table-bordered table-striped" id="dtTable">
					<thead>
						<tr>
							<th class="dt_toggle">#</th>
							<th class="dt_name">Имя</th>
							<th class="dt_email">Email</th>
							<th class="dt_phone">Телефон</th>
							<th class="dt_create">Написано</th>
							<th class="dt_update">Обновлено</th>
							<th class="dt_status">Статус</th>
							<th class="dt_actions">Операций</th>
						</tr>
					</thead>
				</table>
			</div>
			<!-- /.box-body -->
		</form>
	</div>
</div>
<script>
	var dtTable; //obj of datatable
	$(function(){
		dtTable = $('#dtTable').dataTable( {
			language: {
				url: "/theme/admin/plugins/datatable/js/Russian.json"
			},
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"bProcessing": true,
			"bServerSide": true,
			"bSortCellsTop": true,
			"sAjaxSource": base_url + "admin/contacts/ajax_operations/list_dt",
			"sServerMethod": "POST",
			"iDisplayLength": 100,
			"aoColumnDefs": [
				{ "sClass": "w-50 text-center vam", "aTargets": ["dt_toggle"], "mData": "dt_toggle" , "bSortable": false},
				{ "sClass": "text-left vam", "aTargets": ["dt_name"], "mData": "dt_name" , "bSortable": false},
				{ "sClass": "w-200 text-left vam", "aTargets": ["dt_email"], "mData": "dt_email" , "bSortable": false},
				{ "sClass": "w-200 text-left vam", "aTargets": ["dt_phone"], "mData": "dt_phone" , "bSortable": false},
				{ "sClass": "w-120 text-left vam", "aTargets": ["dt_create"], "mData": "dt_create"},
				{ "sClass": "w-120 text-left vam", "aTargets": ["dt_update"], "mData": "dt_update"},
				{ "sClass": "w-100 text-center vam", "aTargets": ["dt_status"], "mData": "dt_status"},
				{ "sClass": "w-120 text-left vam", "aTargets": ["dt_detail"], "mData": "dt_detail" , "bSortable": false},
				{ "sClass": "w-95 text-center vam", "aTargets": ["dt_actions"], "mData": "dt_actions" , "bSortable": false }

			],
			"aaSorting" : [[4,'desc']],
			"fnServerData": function ( sSource, aoData, fnCallback ) {
				$.ajax( {
					"dataType": 'JSON',
					"type": "POST",
					"url": sSource,
					"data": aoData,
					"success": function (data, textStatus, jqXHR) {
						if(data.mess_type != 'success'){
							systemMessages(data.message, 'message-' + data.mess_type);
						}

						fnCallback(data, textStatus, jqXHR);
					}
				});
			},
			"sPaginationType": "full_numbers",
			"fnDrawCallback": function( oSettings ) {
				if (Math.ceil((this.fnSettings().fnRecordsDisplay()) / this.fnSettings()._iDisplayLength) > 1) {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "block"); 
                    $('#dtTable_wrapper .dataTables_filter').css("display", "block"); 
                } else {
                    $('#dtTable_wrapper .dataTables_paginate').css("display", "none");
                    $('#dtTable_wrapper .dataTables_filter').css("display", "none");
                }
			}
		});

		$('body').on('click', 'span.order_details', function(e) {
            e.preventDefault();
			var $thisBtn = $(this);
		    var nTr = $thisBtn.parents('tr')[0];

			if (dtTable.fnIsOpen(nTr))
				dtTable.fnClose(nTr);
		    else
				dtTable.fnOpen(nTr, fnFormatDetails(nTr), 'details');

			$thisBtn.find('.ca-icon').toggleClass('ca-icon_plus ca-icon_minus');
			$('input').iCheck({
				checkboxClass: 'icheckbox_flat-blue',
				radioClass: 'iradio_flat-blue'
			})
		});
	});

	function fnFormatDetails(nTr){
		var aData = dtTable.fnGetData(nTr);
		var sOut = aData['dt_detail'];
		return sOut;
	}

	var toggle_detail = function(obj){
		var $this = $(obj);
		var nTr = $this.closest('tr')[0];

		if (dtTable.fnIsOpen(nTr)){
			dtTable.fnClose(nTr);
		} else{
			dtTable.fnOpen(nTr, fnFormatDetails(nTr), 'details');
		}

		$this.find('.fa').toggleClass('fa-plus fa-minus');
	}

	var delete_action = function(btn){
		var $this = $(btn);
		var id_contact = $this.data('contact');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/contacts/ajax_operations/delete',
			data: {contact:id_contact},
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}

	var change_status = function(btn){
		var $this = $(btn);
		var status = $this.data('status');
		var btn_color = $this.data('color');
		var btn_text = $this.data('text');
		$this.closest('td').find('input[name="contact[status]"]').val(status);
		var template = '<button type="button" class="btn btn-'+btn_color+' btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
							'+btn_text+' <span class="caret"></span>\
                        </button>';
		$this.closest('.btn-group').find('button').replaceWith(template);
		$this.closest('.btn-group').toggleClass('open');
		return false;
	}

	var change_contact = function(btn){
		var $this = $(btn);
		var $form = $this.closest('form');
		$.ajax({
			type: 'POST',
			url: base_url+'admin/contacts/ajax_operations/change',
			data: $form.serialize(),
			dataType: 'JSON',
			success: function(resp){
				systemMessages(resp.message, resp.mess_type);
				if(resp.mess_type == 'success'){
					dtTable.fnDraw(false);
				}
			}
		});
		return false;
	}
</script>
