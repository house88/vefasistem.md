<form>
    <table class="table">
        <tbody>
            <?php $statuses = get_contact_statuses();?>
            <tr>
                <td class="col-xs-3 vam">
                    Статус
                </td>
                <td class="col-xs-9 vam text-left">
                    <input type="hidden" name="contact[status]" class="form-control input-sm" value="<?php echo $contact['contact_status'];?>">
                    <div class="btn-group dropdown">
                        <button type="button" class="btn btn-<?php echo $statuses[$contact['contact_status']]['color_class']?> btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $statuses[$contact['contact_status']]['title']?> <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <?php foreach($statuses as $status_key => $status){?>
                                <li><a href="#" class="call-function" data-toggle="dropdown" data-callback="change_status" data-status="<?php echo $status_key;?>" data-color="<?php echo $status['color_class'];?>" data-text="<?php echo $status['title'];?>"><strong class="text-<?php echo $status['color_class'];?>"><?php echo $status['title'];?></strong></a></li>
                            <?php }?>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="col-xs-3 vam">
                    Сообшение клиента
                </td>
                <td class="col-xs-9 vam text-left">
					<?php echo $contact['contact_text_client'];?>
                </td>
            </tr>
            <tr>
                <td class="col-xs-3 vam">
                    Коментарий администратора
                </td>
                <td class="col-xs-9 vam text-left">
					<textarea name="contact[admin_comment]" rows="3" class="form-control w-100pr_i"><?php echo $contact['contact_text_admin'];?></textarea>
                </td>
            </tr>
            <tr>
                <td class="col-xs-12 vam text-right" colspan="2">
					<input type="hidden" name="contact[id_contact]" value="<?php echo $contact['id_contact'];?>">
                    <button type="button" class="btn btn-success call-function" data-callback="change_contact" data-contact="<?php echo $contact['id_contact'];?>">Сохранить изменения</button>
                </td>
            </tr>
        </tbody>
    </table>
</form>