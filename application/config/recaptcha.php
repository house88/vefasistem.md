<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6Lf6gRcUAAAAAOgzdCF-e_dX8CkNcOUndBUTBJcP';
$config['recaptcha_secret_key'] = '6Lf6gRcUAAAAAD9lo0HxZS89PQpUw5Iyxki-mSGG';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'ru';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
