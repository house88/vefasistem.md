function showLoader(loaderElement,lodaerText){
	if(lodaerText==undefined)
		lodaerText = 'Обработка данных...';

	var $this = $(loaderElement).children('.ajax-loader');

	if ($this.length > 0)
		$this.show();
	else {
		
		$(loaderElement).prepend('<div class="ajax-loader">\
									<div class="wrapper">\
										<div class="bounce1"></div>\
										<div class="bounce2"></div>\
										<div class="bounce3"></div>\
									</div>\
								</div>');
		$(loaderElement).children('.ajax-loader').show();
	}
}

function hideLoader(parentId){
	var $this = $(parentId).children('.ajax-loader');

	if ($this.length > 0)
		$this.hide();
}

function systemMessages(mess_array, ul_class){
	typeM = typeof mess_array;
	var good_array = [];
	switch(typeM){
		case 'string': 
			good_array = [mess_array]; 
		break;
		case 'array': 
			good_array = mess_array; 
		break;
		case 'object': 
			for(var i in mess_array){
				good_array.push(mess_array[i]);
			}
		break;
		
	}
	
	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	var $systMessUl = $('.system-text-messages-b ul');
	$systMessUl.html('');
	for (var li in good_array ) {
		$systMessUl.prepend('<li  class="message-' + ul_class + '">'+ good_array[li] +' <i class="fa fa-remove"></i></li>');
		$systMessUl.children('li').first().addClass('zoomIn').show().delay( 30000 ).slideUp('slow', function(){
			if($systMessUl.children('li').length == 1){
				$systMessUl.closest('.system-text-messages-b').slideUp();
				$(this).remove();
			}else
				$(this).remove();
		});
	}
}

function clearSystemMessages(){	
	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	$('.system-text-messages-b ul').html('');
}

$(function(){
    $('body').on("click", '.system-text-messages-b li .fa-remove', function(){
        var $li = $(this).closest('li');
        $li.clearQueue();
        $li.slideUp('slow',function(){
            if($('.system-text-messages-b li').length == 1){
                $('.system-text-messages-b').slideUp();
                $li.remove();
            }else
                $li.remove();
        });
    });

	$('body').on('mouseover mouseenter', '*[title]', function(){
		if(!$(this).hasClass('tooltipstered')){
			$(this).tooltipster();
		}
		$(this).tooltipster('show');
    });
	
	$("body").on('click', '.confirm-dialog',function(e){
		var $thisBtn = $(this);
		e.preventDefault();

		BootstrapDialog.show({
			title: $thisBtn.data('title'),
			message: $thisBtn.data('message'),
			closable: false,
			draggable: true,
			buttons: [{
				label: 'Да',
				cssClass: 'btn-success',
				action: function(dialogRef){
					var callBack = $thisBtn.data('callback');
					var $button = this;
                    $button.disable();

					window[callBack]($thisBtn);
					dialogRef.close();
				}
			},
			{
				label: 'Отменить',
				cssClass: 'btn-danger',
				action: function(dialogRef){
					dialogRef.close();
				}
			}]
		});
	});
	
	$('body').on('click', ".call-function", function(e){
		e.preventDefault();
		var $thisBtn = $(this);
		if($thisBtn.hasClass('disabled')){
			return false;
		}
		
		var callBack = $thisBtn.data('callback');
		window[callBack]($thisBtn);
		return false;
	});

    var url = window.location;
    var element = $('ul#side-menu a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).parent('li').addClass('active').parent().parent().addClass('active');
    
    if (element.is('li')) {
        element.addClass('active');
    }
});