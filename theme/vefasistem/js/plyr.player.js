var video_player;
var current_index = 1;
var players = null;
var play_video = function(btn){
    var $this = $(btn);

    if($this.data('index')){
        current_index = $this.data('index');
    }

    $('[data-index]').removeClass('active');
    $('[data-index="'+current_index+'"]').addClass('active');

    $('.video-wr').fadeIn('slow', function(){
        $('.trocal76_wr .full-wr').hide();
    });

    video_player_callback('pause');
    video_player.source({
        type: 'video',
        sources:[
            {
                src: base_url+'files/trocal76_video/'+site_lang+'/'+current_index+'.mp4',
                type: 'video/mp4'
            }
        ],
        poster:base_url+'files/trocal76_video/'+site_lang+'/'+current_index+'.jpg'
    });
    video_player_callback('play');
}

var video_stop = function(){
    video_player_callback('stop');
}

function video_player_callback(action){
    switch (action) {
        case 'restart':
            video_player.restart();
        break;    
        case 'stop':
            if(video_player.isReady() && !video_player.isPaused()){
                video_player.stop();
                video_player.destroy();
                players = plyr.setup('#trocal76_player-element', video_options);    
                video_player = players[0];
                video_player.source({
                    type: 'video',
                    sources:[
                        {
                            src: base_url+'files/trocal76_video/'+site_lang+'/'+current_index+'.mp4',
                            type: 'video/mp4'
                        }
                    ],
                    poster:base_url+'files/trocal76_video/'+site_lang+'/'+current_index+'.jpg'
                });
                video_player.on('ended', function(event) {
                    if(video_player.getCurrentTime() >= video_player.getDuration()){
                        video_player_callback('stop');
                    }
                    
                    $('.video_description > div[data-video]').hide();
                    $('.video_description > div[data-video="'+current_index+'"]').show();

                    $('.trocal76_wr').addClass('video-skiped');
            
                    console.log('ended');
                }).on('play', function(event) {
                    console.log('playing');
                });
                    
                $('.video_description > div[data-video]').hide();
                $('.video_description > div[data-video="'+current_index+'"]').show();

                $('.trocal76_wr').addClass('video-skiped');
            }
        break;    
        case 'play':
            if(video_player.isReady()){
                video_player.play();
                $('.trocal76_wr').removeClass('video-skiped');
            }
        break;    
        default:
        case 'pause':
            if(!video_player.isPaused()){
                video_player.pause();
            }
        break;
    }
}

var controls = [
    "<div class='plyr__controls'>",
        "<span class='btn btn3d btn-hot btn-lg call-function' data-callback='video_stop'>",
            "Skip video",
        "</span>",
        "<span class='btn btn3d btn-sky btn-lg call-function' data-callback='play_video'>",
            "Play video",
        "</span>",
    "</div>"
].join("");

var video_options = {
    debug: false,
    html:controls,
    showPosterOnEnd:true
}

$(function(){
    'use strict';

    players = plyr.setup('#trocal76_player-element', video_options);    
    video_player = players[0];
    video_player.on('ended', function(event) {
        if(video_player.getCurrentTime() >= video_player.getDuration()){
            video_player_callback('stop');
        }

        
        $('.video_description > div[data-video]').hide();
        $('.video_description > div[data-video="'+current_index+'"]').show();

        $('.trocal76_wr').addClass('video-skiped');

        console.log('ended');
    }).on('play', function(event) {
        console.log('playing');
    });
});