$(function(){
    'use strict';
    
    $('body').on("click", '.system-text-messages-b li .fa-remove', function(){
        var $li = $(this).closest('li');
        $li.clearQueue();
        $li.slideUp('slow',function(){
            if($('.system-text-messages-b li').length == 1){
                $('.system-text-messages-b').slideUp();
                $li.remove();
            }else
                $li.remove();
        });
    });
    
    $('body').on('click', ".call-function", function(e){
        e.preventDefault();
        var $thisBtn = $(this);
        var callBack = $thisBtn.data('callback');
        window[callBack]($thisBtn);
        return false;
    });

	$('body').on('click', ".call-popup", function(e){
		e.preventDefault();
		var $this = $(this);
		if($this.hasClass('disabled')){
			return false;
		}

        var inst = $('[data-remodal-id="'+$this.data('popup')+'"]').remodal();
        var popup_url = $this.data('href');
		var $popup_parent = $('[data-remodal-id="'+$this.data('popup')+'"]');
		$.ajax({
			type: 'POST',
			url: popup_url,
			data: {},
			dataType: 'JSON',
			beforeSend: function(){
				clearSystemMessages();
			},
			success: function(resp){
				if(resp.mess_type == 'success'){
                    $popup_parent.find('.modal-content-wr').html(resp.popup_content);

                    $('.modal-content-wr input.nice-input').icheck({
                        checkboxClass: 'icheckbox_flat-blue',
                        radioClass: 'iradio_flat-blue'
                    });
                
                    $('.modal-content-wr input.nice-input[name="add_comment"]').on('ifChecked', function(event){
                        $(this).closest('form').find('.contact_us_message').show();
                    }).on('ifUnchecked', function(event){
                        $(this).closest('form').find('.contact_us_message').hide();
                    });

                    inst.open();
				} else{
					systemMessages( resp.message, resp.mess_type );
				}
			}
		});
		return false;
	});
    
	setTimeout(function() {
        $('body').addClass('loaded');
    }, 300);

    headerColor();

    $('input.nice-input').icheck({
		checkboxClass: 'icheckbox_flat-blue',
		radioClass: 'iradio_flat-blue'
	});

    $('input.nice-input[name="add_comment"]').on('ifChecked', function(event){
        $(this).closest('form').find('.contact_us_message').show();
    }).on('ifUnchecked', function(event){
        $(this).closest('form').find('.contact_us_message').hide();
    });

    var waypoint = new Waypoint({
        element: document.getElementById('why-we_wr'),
        handler: function(direction) {    
            if (direction === 'down') {
                countAnimation()
            }
        },
        offset: '50%',
        triggerOnce:true
    });

    // Progress slider
    //-----------------------------------------------------------------------------------
    var progressSlider = $('#windows_evolution');

    $(window).on('load debounce resize', function () {
        if ($(window).width() < 1184) {
            progressSlider.slick({
                infinite:       false,
                arrows:         false,
                centerMode:     true,
                slidesToShow:   1,
                slidesToScroll: 1
            });
        } else {
            if(progressSlider.hasClass('slick-initialized')) progressSlider.slick('unslick');
        }
    });

    $(document).on('click', '.windows_evolution-prev', function (e) {
        e.preventDefault();
        progressSlider.slick('slickPrev');
    });

    $(document).on('click', '.windows_evolution-next', function (e) {
        e.preventDefault();
        progressSlider.slick('slickNext');
    });

    $('.nav-show-menu').click(function(){
		var $this = $(this);
        var is_opened = $this.hasClass('open');
        $this.toggleClass('open');
        if(!is_opened){
            $('body').addClass('scroll-block');
            $('.js-menu').addClass('show');
        } else{
            $('body').removeClass('scroll-block');
            $('.js-menu').removeClass('show');
        }
	});
});
$(document).on('click', function (e) {
    if (!e.target.closest('.js-menu, .nav-show-menu')) {
        $('.nav-show-menu').removeClass('open');
        $('body').removeClass('scroll-block');
        $('.js-menu').removeClass('show');
    }
});

var countAnimationExecuted = false;
function countAnimation(){
    if (!countAnimationExecuted) {
        countAnimationExecuted = true;
        var _countList = $('[data-count]');
        $.each(_countList, function (value, index) {
            var endValue = $(this).data('number');

            var queueCountAnimation = new CountUp($(this).data('target'), 0, endValue, 0, 2);
            queueCountAnimation.start();
        });
    }
}

var modal = $('.details').remodal();

// Header in white
//-----------------------------------------------------------------------------------
    
$(window).scroll(function () {
    headerColor();
});

function headerColor(){
    var $header       = $('#header_nav');
    var $firstSection = $('.header-wr').outerHeight();
        
    if ($(window).width() > 767) {
        if ($(window).scrollTop() > ($firstSection - 110)) {
            $header.addClass('mode-white');
        } else {
            $header.removeClass('mode-white');
        }
    } else{
        if ($(window).scrollTop() > ($firstSection - 61)) {
            $header.addClass('mode-white');
        } else {
            $header.removeClass('mode-white');
        }
    }
}

// Scroll to link
//-----------------------------------------------------------------------------------
$(document).on('click', '.js-scroll[href*=#]', function (e) {
    e.preventDefault();
    $('.nav-show-menu').removeClass('open');
    $('body').removeClass('scroll-block');
    $('.js-menu').removeClass('show');
    var scrolling = $($(this).attr('href')).offset().top - 110;
    if ($(window).width() < 768){
        scrolling = scrolling + 50;
    }

    $('html, body').stop().animate({
        scrollTop: scrolling
    }, 1000);
});

function systemMessages(mess_array, ul_class){
	typeM = typeof mess_array;
	var good_array = [];
	switch(typeM){
		case 'string': 
			good_array = [mess_array]; 
		break;
		case 'array': 
			good_array = mess_array; 
		break;
		case 'object': 
			for(var i in mess_array){
				good_array.push(mess_array[i]);
			}
		break;
		
	}
	
	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	var $systMessUl = $('.system-text-messages-b ul');
	$systMessUl.html('');
	for (var li in good_array ) {
		$systMessUl.prepend('<li  class="message-' + ul_class + '">'+ good_array[li] +' <i class="fa fa-remove"></i></li>');
		$systMessUl.children('li').first().addClass('zoomIn').show().delay( 300000 ).slideUp('slow', function(){
			if($systMessUl.children('li').length == 1){
				$systMessUl.closest('.system-text-messages-b').slideUp();
				$(this).remove();
			}else
				$(this).remove();
		});
	}
}

function clearSystemMessages(){	
	if (!$('.system-text-messages-b').is(':visible')){
		$( '.system-text-messages-b' ).fadeIn('fast');
	}
	$('.system-text-messages-b ul').html('');
}

var callme_callback = function(obj){
    var $this = $(obj);
    var $form = $this.closest('form');
    $.ajax({
        type: 'POST',
        url: base_url+site_lang+'/contacts/ajax_operations/callme',
        data: $form.serialize(),
        dataType: 'JSON',
        beforeSend: function(){
            clearSystemMessages();
        },
        success: function(resp){
            systemMessages( resp.message, resp.mess_type );
            if(resp.mess_type == 'success'){
                $form[0].reset();
                var inst = $('[data-remodal-id="contact_us"]').remodal();
                inst.close();
            }
        }
    });
    return false;
}